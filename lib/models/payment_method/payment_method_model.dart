import 'package:json_annotation/json_annotation.dart';
import 'package:vf_prototype_flutter/models/selectable_interface.dart';

import '../../utils/utils.dart';
part 'payment_method_model.g.dart';

@JsonSerializable()
class PaymentMethodModel implements Selectable {
  @JsonKey(fromJson: Utils.stringToInt, name: "id_payment_method")
  final int idPaymentMethod;
  @JsonKey(fromJson: Utils.stringToInt, name: "type")
  final int type;
  @JsonKey(name: "name")
  final String name;

  factory PaymentMethodModel.fromJson(Map<String, dynamic> json) =>
      _$PaymentMethodModelFromJson(json);

  PaymentMethodModel(this.idPaymentMethod, this.type, this.name);

  @override
  String toSelectString() {
    return name;
  }

  @override
  String toString() {
    return toSelectString();
  }
}
