import 'package:json_annotation/json_annotation.dart';
import 'package:vf_prototype_flutter/models/selectable_interface.dart';

import '../../utils/utils.dart';
part 'number_serie_model.g.dart';

@JsonSerializable()
class NumberSerieModel implements Selectable {
  @JsonKey(fromJson: Utils.stringToInt, name: "id_number_series")
  late int idNumberSerie = 0;
  @JsonKey(fromJson: Utils.stringToInt, name: "type")
  late int type = 0;
  @JsonKey(name: "name")
  late String name = "";

  NumberSerieModel(this.idNumberSerie, this.type, this.name);

  NumberSerieModel.empty();

  factory NumberSerieModel.fromJson(Map<String, dynamic> json) =>
      _$NumberSerieModelFromJson(json);
  Map<String, dynamic> toJson() => _$NumberSerieModelToJson(this);

  @override
  String toSelectString() {
    return name;
  }
  
  @override
  String toString() {
    return toSelectString();
  }
}
