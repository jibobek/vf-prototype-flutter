abstract class Selectable {
  String toSelectString();

  @override
  String toString() {
    return toSelectString();
  }
}
