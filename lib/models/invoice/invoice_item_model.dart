import 'package:json_annotation/json_annotation.dart';

import '../../utils/utils.dart';
part 'invoice_item_model.g.dart';

@JsonSerializable(explicitToJson: true)
class InvoiceItemModel {
  @JsonKey(fromJson: Utils.stringToDouble, name: "quantity")
  double quantity = 1.0;
  @JsonKey(name: "unit")
  String unit = "ks";
  @JsonKey(name: "text")
  String text = "";
  @JsonKey(fromJson: Utils.stringToInt, name: "vat_rate")
  int vatRate = 21;
  @JsonKey(fromJson: Utils.stringToDouble, name: "unit_price")
  double unitPrice = 0.0;

  InvoiceItemModel.empty();

  InvoiceItemModel(
      {required this.quantity,
      required this.unit,
      required this.text,
      required this.vatRate,
      required this.unitPrice});

  factory InvoiceItemModel.fromJson(Map<String, dynamic> json) =>
      _$InvoiceItemModelFromJson(json);
  Map<String, dynamic> toJson() => _$InvoiceItemModelToJson(this);

  @override
  String toString() {
    return toJson().toString();
  }
}
