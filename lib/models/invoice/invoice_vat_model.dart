import 'package:json_annotation/json_annotation.dart';

part 'invoice_vat_model.g.dart';

@JsonSerializable()
class InvoiceVatModel {
  @JsonKey(name: "vat_rate")
  final int vatRate;
  @JsonKey(name: "base")
  final double base;
  @JsonKey(name: "vat")
  final double vat;
  @JsonKey(name: "total")
  final double total;

  InvoiceVatModel(
      {required this.vatRate,
      required this.base,
      required this.vat,
      required this.total});

  factory InvoiceVatModel.fromJson(Map<String, dynamic> json) =>
      _$InvoiceVatModelFromJson(json);
  Map<String, dynamic> toJson() => _$InvoiceVatModelToJson(this);
}
