import 'package:json_annotation/json_annotation.dart';
import 'package:vf_prototype_flutter/models/invoice/invoice_vat_model.dart';
import 'package:vf_prototype_flutter/utils/currency_utils.dart';

import '../../utils/utils.dart';
import 'invoice_item_model.dart';
part 'invoice_model.g.dart';

@JsonSerializable()
class InvoiceModel {
  @JsonKey(fromJson: Utils.stringToInt, name: "id")
  int idInvoice = 0;
  @JsonKey(fromJson: Utils.stringToInt, name: "id_customer")
  int idCustomer = 0;
  @JsonKey(fromJson: Utils.stringToInt, name: "id_number_series")
  int idNumberSeries = 0;
  @JsonKey(fromJson: Utils.stringToInt, name: "id_payment_method")
  int idPaymentMethod = 0;
  @JsonKey(fromJson: Utils.stringToInt, name: "type")
  int type = 0;
  @JsonKey(fromJson: Utils.stringToInt, name: "flags")
  int flags = 0;
  @JsonKey(name: "number")
  String number = "";
  @JsonKey(name: "date_created")
  String dateCreated = "";
  @JsonKey(name: "date_due")
  String dateDue = "";
  @JsonKey(name: "date_taxable_supply")
  String dateTaxableSupply = "";
  @JsonKey(name: "date_paid")
  String datePaid = "";
  @JsonKey(fromJson: Utils.stringToInt, name: "days_due")
  int daysDue = 10;
  @JsonKey(name: "supplier_IC", toJson: Utils.toNull, includeIfNull: false)
  String supplierIc = "";
  @JsonKey(name: "supplier_DIC", toJson: Utils.toNull, includeIfNull: false)
  String supplierDic = "";
  @JsonKey(name: "supplier_IDNUM3", toJson: Utils.toNull, includeIfNull: false)
  String supplierIdnum3 = "";
  @JsonKey(name: "supplier_name", toJson: Utils.toNull, includeIfNull: false)
  String supplierName = "";
  @JsonKey(name: "supplier_street", toJson: Utils.toNull, includeIfNull: false)
  String supplierStreet = "";
  @JsonKey(name: "supplier_city", toJson: Utils.toNull, includeIfNull: false)
  String supplierCity = "";
  @JsonKey(name: "supplier_zip", toJson: Utils.toNull, includeIfNull: false)
  String supplierZip = "";
  @JsonKey(
      name: "supplier_country_code", toJson: Utils.toNull, includeIfNull: false)
  String supplierCountryCode = "";
  @JsonKey(name: "customer_IC")
  String customerIc = "";
  @JsonKey(name: "customer_DIC")
  String customerDic = "";
  @JsonKey(name: "customer_IDNUM3")
  String customerIdnum3 = "";
  @JsonKey(name: "customer_name")
  String customerName = "";
  @JsonKey(name: "customer_firstname")
  String customerFirstname = "";
  @JsonKey(name: "customer_lastname")
  String customerLastname = "";
  @JsonKey(name: "customer_street")
  String customerStreet = "";
  @JsonKey(name: "customer_city")
  String customerCity = "";
  @JsonKey(name: "customer_zip")
  String customerZip = "";
  @JsonKey(name: "customer_country_code")
  String customerCountryCode = "";
  @JsonKey(name: "bank_account_number")
  String bankAccountNumber = "";
  @JsonKey(name: "language")
  String language = "";
  @JsonKey(name: "VS")
  String vs = "";
  @JsonKey(name: "SS")
  String ss = "";
  @JsonKey(name: "KS")
  String ks = "";
  @JsonKey(name: "currency")
  String currency = "";
  @JsonKey(name: "total")
  String total = "";
  @JsonKey(name: "total_without_vat")
  String totalWithoutVat = "";
  @JsonKey(name: "items")
  List<InvoiceItemModel> items = [];
  @JsonKey(name: "url_public_webpage")
  String urlPublicWebpage = "";
  @JsonKey(name: "vats")
  List<InvoiceVatModel> vats = [];

  InvoiceModel.empty() {
    CurrencyUtils currencyUtils = CurrencyUtils();
    currency = currencyUtils.defaultCurrency.name;
  }

  InvoiceModel({
    required this.idInvoice,
    required this.idCustomer,
    required this.idNumberSeries,
    required this.idPaymentMethod,
    required this.type,
    required this.flags,
    required this.number,
    required this.dateCreated,
    required this.dateDue,
    required this.dateTaxableSupply,
    required this.datePaid,
    required this.daysDue,
    required this.supplierIc,
    required this.supplierDic,
    required this.supplierIdnum3,
    required this.supplierName,
    required this.supplierStreet,
    required this.supplierCity,
    required this.supplierZip,
    required this.supplierCountryCode,
    required this.customerIc,
    required this.customerDic,
    required this.customerIdnum3,
    required this.customerName,
    required this.customerFirstname,
    required this.customerLastname,
    required this.customerStreet,
    required this.customerCity,
    required this.customerZip,
    required this.customerCountryCode,
    required this.bankAccountNumber,
    required this.language,
    required this.vs,
    required this.ss,
    required this.ks,
    required this.currency,
    required this.total,
    required this.totalWithoutVat,
    required this.items,
    required this.urlPublicWebpage,
    required this.vats,
  });

  bool isStorno() {
    return flags & 8 > 0;
  }

  bool isArchived() {
    return flags & 4096 > 0;
  }

  factory InvoiceModel.fromJson(Map<String, dynamic> json) =>
      _$InvoiceModelFromJson(json);
  Map<String, dynamic> toJson() => _$InvoiceModelToJson(this);
}
