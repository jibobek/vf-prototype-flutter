import 'package:vf_prototype_flutter/models/login/company_model.dart';

import 'package:json_annotation/json_annotation.dart';
import 'package:vf_prototype_flutter/utils/utils.dart';
part 'login_model.g.dart';

@JsonSerializable(explicitToJson: true)
class LoginModel {
  @JsonKey(
      fromJson: Utils.stringToInt,
      name: 'id_company_last_used')
  final int lastCompany;
  @JsonKey(name: "companies")
  final List<CompanyModel> companies;

  LoginModel(this.lastCompany, this.companies);

  factory LoginModel.fromJson(Map<String, dynamic> json) =>
      _$LoginModelFromJson(json);
  Map<String, dynamic> toJson() => _$LoginModelToJson(this);

}
