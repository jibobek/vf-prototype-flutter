import 'package:json_annotation/json_annotation.dart';

import '../../utils/utils.dart';
part 'company_model.g.dart';

@JsonSerializable()
class CompanyModel {
  @JsonKey(fromJson: Utils.stringToInt, name: "id_company")
  final int idCompany;
  @JsonKey(fromJson: Utils.stringToInt, name: "flags")
  final int flags;
  @JsonKey(name: "IC")
  final String? ic;
  @JsonKey(name: "DIC")
  final String? dic;
  @JsonKey(name: "name")
  final String name;
  @JsonKey(name: "api_key")
  final String apiKey;

  CompanyModel(
      this.idCompany, this.flags, this.ic, this.dic, this.name, this.apiKey);

  factory CompanyModel.fromJson(Map<String, dynamic> json) =>
      _$CompanyModelFromJson(json);
  Map<String, dynamic> toJson() => _$CompanyModelToJson(this);
}
