import 'package:json_annotation/json_annotation.dart';
import 'package:vf_prototype_flutter/models/settings/settings_model.dart';

part 'autologin_model.g.dart';

@JsonSerializable()
class AutoLoginModel {
  @JsonKey(name: "settings")
  final SettingsModel settings;

  AutoLoginModel(this.settings);

  factory AutoLoginModel.fromJson(Map<String, dynamic> json) =>
      _$AutoLoginModelFromJson(json);
}
