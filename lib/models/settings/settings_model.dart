import 'package:json_annotation/json_annotation.dart';

import '../login/company_model.dart';
import '../number_serie/number_serie_model.dart';
import '../payment_method/payment_method_model.dart';
part 'settings_model.g.dart';

@JsonSerializable()
class SettingsModel {
  @JsonKey(name: "companies")
  final List<CompanyModel> companies;
  @JsonKey(name: "numberSeries")
  final List<NumberSerieModel> numberSeries;
  @JsonKey(name: "paymentMethods")
  final List<PaymentMethodModel> paymentMethods;

  SettingsModel(this.companies, this.numberSeries, this.paymentMethods);

  List<NumberSerieModel> getNumberSeriesByType(int type) {
    return numberSeries.where((element) => element.type == type).toList();
  }

  factory SettingsModel.fromJson(Map<String, dynamic> json) =>
      _$SettingsModelFromJson(json);
  Map<String, dynamic> toJson() => _$SettingsModelToJson(this);
}
