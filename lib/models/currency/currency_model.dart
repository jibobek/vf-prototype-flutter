import 'package:vf_prototype_flutter/models/selectable_interface.dart';

class CurrencyModel implements Selectable {
  String name = "";
  String symbol = "";

  CurrencyModel({required this.name, required this.symbol});

  @override
  String toSelectString() {
    return name;
  }
  
  @override
  String toString() {
    return toSelectString();
  }
}
