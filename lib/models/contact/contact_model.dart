import 'package:json_annotation/json_annotation.dart';
import 'package:vf_prototype_flutter/models/selectable_interface.dart';

import '../../utils/utils.dart';
part 'contact_model.g.dart';

@JsonSerializable(explicitToJson: true)
class ContactModel implements Selectable {
  @JsonKey(fromJson: Utils.stringToInt, name: "id")
  int idContact = 0;
  @JsonKey(name: "IC")
  String ic = "";
  @JsonKey(name: "DIC")
  String dic = "";
  @JsonKey(name: "IDNUM3")
  String idnum3 = "";
  @JsonKey(name: "name")
  String name = "";
  @JsonKey(name: "company")
  String company = "";
  @JsonKey(name: "firstname")
  String firstname = "";
  @JsonKey(name: "lastname")
  String lastname = "";
  @JsonKey(name: "street")
  String street = "";
  @JsonKey(name: "city")
  String city = "";
  @JsonKey(name: "zip")
  String zip = "";
  @JsonKey(name: "country_code")
  String countryCode = "";
  @JsonKey(name: "delivery_company")
  String deliveryCompany = "";
  @JsonKey(name: "delivery_firstname")
  String deliveryFirstname = "";
  @JsonKey(name: "delivery_lastname")
  String deliveryLastname = "";
  @JsonKey(name: "delivery_street")
  String deliveryStreet = "";
  @JsonKey(name: "delivery_city")
  String deliveryCity = "";
  @JsonKey(name: "delivery_zip")
  String deliveryZip = "";
  @JsonKey(name: "delivery_country_code")
  String deliveryCountryCode = "";
  @JsonKey(name: "delivery_tel")
  String deliveryTel = "";
  @JsonKey(name: "text_under_subscriber")
  String textUnderSubscriber = "";
  @JsonKey(name: "text_before_items")
  String textBeforeItems = "";
  @JsonKey(name: "text_invoice_footer")
  String textInvoiceFooter = "";
  @JsonKey(name: "note")
  String note = "";
  @JsonKey(name: "mail_to")
  String email = "";

  ContactModel.empty();

  ContactModel(
      this.idContact,
      this.ic,
      this.dic,
      this.idnum3,
      this.name,
      this.company,
      this.firstname,
      this.lastname,
      this.street,
      this.city,
      this.zip,
      this.countryCode,
      this.deliveryCompany,
      this.deliveryFirstname,
      this.deliveryLastname,
      this.deliveryStreet,
      this.deliveryCity,
      this.deliveryZip,
      this.deliveryCountryCode,
      this.deliveryTel,
      this.textUnderSubscriber,
      this.textBeforeItems,
      this.textInvoiceFooter,
      this.note,
      this.email);

  factory ContactModel.fromJson(Map<String, dynamic> json) =>
      _$ContactModelFromJson(json);
  Map<String, dynamic> toJson() => _$ContactModelToJson(this);

  @override
  String toSelectString() {
    return name;
  }
  
  @override
  String toString() {
    return toSelectString();
  }
}
