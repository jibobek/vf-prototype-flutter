import 'package:vf_prototype_flutter/models/currency/currency_model.dart';

class CurrencyUtils {
  final List<CurrencyModel> currencies = [];
  late final CurrencyModel defaultCurrency;

  CurrencyUtils() {
    currencies.addAll([
      CurrencyModel(name: "CZK", symbol: "Kč"),
      CurrencyModel(name: "EUR", symbol: "€"),
      CurrencyModel(name: "GPB", symbol: "£"),
      CurrencyModel(name: "USD", symbol: "\$"),
    ]);

    defaultCurrency = currencies.first;
  }

  CurrencyModel getCurrencyFromString(String code) {
    return currencies.firstWhere((element) => element.name == code);
  }

  String getCurrencySymbol(String currencyKey) {
    List<CurrencyModel> validCurrencies =
        currencies.where((element) => element.name == currencyKey).toList();
    if (validCurrencies.isEmpty) {
      return "";
    }
    return validCurrencies.first.symbol;
  }
}
