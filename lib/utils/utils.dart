class Utils {
  static int stringToInt(number) {
    if (number is int) {
      return number;
    }
    if (number is String) {
      return int.parse(number);
    }
    return 0;
  }

  static double stringToDouble(number) {
    if (number is int) {
      return number.toDouble();
    }
    if (number is String) {
      return double.parse(number);
    }
    return 0.0;
  }

  static toNull(_) => null;
}
