class CountryUtils {
  static final Map<String, String> counties = {
    "CZ": "Česká republika",
    "SK": "Slovensko",
    "GB": "Spojené království",
    "US": "USA",
    "PL": "Polsko",
  };

  static String getCountryCodeByName(String name) {
    return counties.entries.firstWhere((c) => c.value == name).key;
  }

  static String getCountryNameByCode(String code) {
    String? name = counties[code];
    if (name == null) {
      return "";
    }
    return name;
  }

  static List<String> getCountryNames() {
    return counties.entries.map((e) => e.value).toList();
  }

  static List<String> getCountryNamesForSelect() {
    List<String> countryNames = getCountryNames();
    countryNames.add("");
    return countryNames;
  }
}
