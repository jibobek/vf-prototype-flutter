import 'package:flutter/material.dart';

class InvoiceUtils {
  static Map<String, Color> getInvoiceBadges(int invoiceFlags) {
    Map<String, Color> badges = {};
    if (invoiceFlags & 1 > 0) {
      badges.addAll({"Obsahuje DPH": Colors.blueGrey});
    }
    if (invoiceFlags & 2 > 0) {
      badges.addAll({"Zaplaceno": Colors.green});
    }
    if (invoiceFlags & 4 > 0) {
      badges.addAll({"Odesláno": Colors.blue});
    }
    if (invoiceFlags & 8 > 0) {
      badges.addAll({"Stornováno": Colors.red});
    }
    if (invoiceFlags & 6 > 0) {
      badges.addAll({"Připomínka": Colors.indigo});
    }
    if (invoiceFlags & 32 > 0) {
      badges.addAll({"Přeplaceno": Colors.yellow});
    }
    if (invoiceFlags & 64 > 0) {
      badges.addAll({"Nedoplaceno": Colors.yellow});
    }
    if (invoiceFlags & 256 > 0) {
      badges.addAll({"Obsahuje Účetní": Colors.black38});
    }
    if (invoiceFlags & 4096 > 0) {
      badges.addAll({"Archivováno": Colors.orange});
    }
    return badges;
  }
}
