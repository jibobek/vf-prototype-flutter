import 'package:jiffy/jiffy.dart';

class ReadableDateUtils {
  static String convertApiDateToReadable(String inputDate) {
    return Jiffy(inputDate, "yyyy-MM-dd").format("dd.MM.yyyy");
  }
}
