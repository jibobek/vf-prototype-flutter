import 'package:flutter/material.dart';

const kIconPadding = EdgeInsets.all(4);

const kContactItemPadding = EdgeInsets.symmetric(horizontal: 4, vertical: 2);

const kInvoiceItemAttributePadding = EdgeInsets.all(16);

const kInvoiceItemBadgesPadding = EdgeInsets.symmetric(vertical: 4, horizontal: 12);
