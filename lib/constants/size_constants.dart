import 'package:flutter/widgets.dart';

const kSizeContainer = BoxConstraints(minWidth: 300, maxWidth: 1024);

const kSizeMobileBreakpoint = 500;
