import 'package:flutter/material.dart';

const kColorMenuBackground = Color.fromRGBO(50,50,50, 1);
const kColorMenuText = Colors.white;
const kColorMenuHover = Colors.white70;
const kColorMenuDivider = Colors.white70;

const kColorToolbarBackground = Colors.black54;

const kColorCardBorder = Colors.black12;

const kColorIcon = Colors.white;

const kColorInputUnderline = Colors.black12;

const kColorInvoiceItemAtribute = Color.fromARGB(209, 7, 7, 7);


const kColorBadgeText = Colors.white;
const kColorButtonText = Colors.white;