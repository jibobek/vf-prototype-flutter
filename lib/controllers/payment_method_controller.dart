import 'package:vf_prototype_flutter/services/storage_service.dart';

import '../models/payment_method/payment_method_model.dart';

class PaymentMethodController {
  PaymentMethodModel? getPaymentMethodById(int paymentMethodId) {
    PaymentMethodModel? paymentMethodModel = StorageService
        .settingsModel!.paymentMethods
        .firstWhere((e) => e.idPaymentMethod == paymentMethodId);
    return paymentMethodModel;
  }
}
