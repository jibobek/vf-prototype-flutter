import 'package:vf_prototype_flutter/models/contact/contact_model.dart';

import '../services/api_endpoint_service.dart';

class ContactController {
  final ApiEndPointService _apiEndPointService = ApiEndPointService();

  Future<void> deleteContact(ContactModel contactModel) async {
    await _apiEndPointService.deleteContact(contactModel);
  }

  Future<ContactModel?> updateContact(ContactModel contactModel) async {
    return await _apiEndPointService.updateContact(contactModel);
  }

  Future<bool> createContact(ContactModel contactModel) async {
    return await _apiEndPointService.createContact(contactModel);
  }

  Future<ContactModel?> getContact(int contactId) async {
    return await _apiEndPointService.getContact(contactId);
  }
}
