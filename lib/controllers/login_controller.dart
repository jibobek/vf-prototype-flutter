import 'package:vf_prototype_flutter/models/login/autologin_model.dart';
import 'package:vf_prototype_flutter/models/login/company_model.dart';
import 'package:vf_prototype_flutter/services/api_endpoint_service.dart';
import 'package:vf_prototype_flutter/services/storage_service.dart';

import '../models/login/login_model.dart';
import 'dart:convert';

class LoginController {
  final ApiEndPointService _apiEndPointService = ApiEndPointService();

  Future<bool> loginWithPassword(String email, String password) async {
    LoginModel? loginModel =
        await _apiEndPointService.loginWithPassword(email, password);
    if (loginModel == null) {
      return false;
    }
    StorageService.changeCurrentCompany(loginModel.lastCompany);
    StorageService.currentUserEmail = email;
    CompanyModel companyModel = loginModel.companies
        .firstWhere((c) => c.idCompany == StorageService.getCurrentCompanyId());
    StorageService.setCurrentApiKey(
        base64.encode(utf8.encode("$email:${companyModel.apiKey}")));
    await trySavedLogin();
    return true;
  }

  Future<bool> trySavedLogin() async {
    await StorageService.loadPernamentStorage();
    AutoLoginModel? autoLoginModel = await _apiEndPointService.testApiHash();
    if (autoLoginModel == null) {
      return false;
    }
    StorageService.settingsModel = autoLoginModel.settings;
    StorageService.changeCurrentCompany(
        autoLoginModel.settings.companies.first.idCompany);
    return true;
  }

  Future<bool> loadCompanyData() async {
    CompanyModel companyModel = StorageService.settingsModel!.companies
        .firstWhere((c) => c.idCompany == StorageService.getCurrentCompanyId());
    StorageService.setCurrentApiKey(base64.encode(utf8
        .encode("${StorageService.currentUserEmail}:${companyModel.apiKey}")));

    AutoLoginModel? autoLoginModel = await _apiEndPointService.testApiHash();
    if (autoLoginModel == null) {
      return false;
    }
    StorageService.settingsModel = autoLoginModel.settings;
    return true;
  }
}
