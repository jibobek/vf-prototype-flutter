import 'package:vf_prototype_flutter/models/contact/contact_model.dart';
import 'package:vf_prototype_flutter/models/invoice/invoice_item_model.dart';
import 'package:vf_prototype_flutter/models/invoice/invoice_model.dart';
import 'package:url_launcher/url_launcher.dart';

import '../services/api_endpoint_service.dart';

class InvoiceController {
  final ApiEndPointService _apiEndPointService = ApiEndPointService();

  Future<void> deleteInvoice(InvoiceModel invoiceModel) async {
    await _apiEndPointService.deleteInvoice(invoiceModel);
  }

  Future<InvoiceModel?> updateInvoice(InvoiceModel invoiceModel) async {
    return await _apiEndPointService.updateInvoice(invoiceModel);
  }

  Future<int?> createInvoice(InvoiceModel invoiceModel) async {
    return await _apiEndPointService.createInvoice(invoiceModel);
  }

  Future<InvoiceModel?> getInvoice(int invoiceId) async {
    return await _apiEndPointService.getInvoice(invoiceId);
  }

  void openWebPage(InvoiceModel invoiceModel) async {
    await launch(invoiceModel.urlPublicWebpage);
  }

  Future<bool> stornoInvoice(InvoiceModel invoiceModel, bool storno) async {
    return await _apiEndPointService.stornoInvoice(invoiceModel, storno);
  }

  Future<bool> archiveInvoice(InvoiceModel invoiceModel, bool archive) async {
    return await _apiEndPointService.archiveInvoice(invoiceModel, archive);
  }

  void addEmptyItem(InvoiceModel invoiceModel) {
    invoiceModel.items.add(InvoiceItemModel.empty());
  }

  InvoiceModel setCustomerToInvoice(
      InvoiceModel invoiceModel, ContactModel contactModel) {
    invoiceModel.customerName = contactModel.name;
    invoiceModel.customerFirstname = contactModel.firstname;
    invoiceModel.customerLastname = contactModel.lastname;
    invoiceModel.customerIdnum3 = contactModel.idnum3;
    invoiceModel.customerStreet = contactModel.street;
    invoiceModel.customerCity = contactModel.city;
    invoiceModel.customerZip = contactModel.zip;
    invoiceModel.customerCountryCode = contactModel.countryCode;
    invoiceModel.customerIc = contactModel.ic;
    invoiceModel.customerDic = contactModel.dic;
    return invoiceModel;
  }
}
