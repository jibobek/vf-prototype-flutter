import 'package:vf_prototype_flutter/models/contact/contact_model.dart';

import '../services/api_endpoint_service.dart';

class ContactListController {
  final ApiEndPointService _apiEndPointService = ApiEndPointService();
  int currentPage = 1;

  Future<List<ContactModel>> getContacts() async {
    List<ContactModel>? contacts =
        await _apiEndPointService.getContacts(page: currentPage);
    if (contacts == null) {
      return [];
    }
    return contacts;
  }

  Future<List<ContactModel>> getContactsByQuery(String searchQuery) async {
    List<ContactModel>? contacts =
        await _apiEndPointService.getContacts(page: currentPage, searchQuery: searchQuery);
    if (contacts == null) {
      return [];
    }
    return contacts;
  }

  Future<List<ContactModel>> getNextContacts() async {
    currentPage++;
    List<ContactModel>? contacts =
        await _apiEndPointService.getContacts(page: currentPage);
    if (contacts == null) {
      return [];
    }
    return contacts;
  }

  Map<String, List<ContactModel>> groupContactsByLetter(
      List<ContactModel> contacts) {
    Map<String, List<ContactModel>> groupedLists = {};
    for (ContactModel contact in contacts) {
      if (groupedLists[contact.name[0]] == null) {
        groupedLists[contact.name[0].toUpperCase()] = <ContactModel>[];
      }
      groupedLists[contact.name[0].toUpperCase()]?.add(contact);
    }
    return groupedLists;
  }
}
