import 'package:vf_prototype_flutter/models/invoice/invoice_model.dart';

import '../services/api_endpoint_service.dart';

class InvoiceListController {
  final ApiEndPointService _apiEndPointService = ApiEndPointService();
  int currentPage = 1;

  Future<List<InvoiceModel>> getInvoices(String searchQuery) async {
    List<InvoiceModel>? invoices = await _apiEndPointService.getInvoices(
        page: currentPage, searchQuery: searchQuery);
    if (invoices == null) {
      return [];
    }
    return invoices;
  }

  Future<List<InvoiceModel>> getNextInvoices(String searchQuery) async {
    currentPage++;
    List<InvoiceModel>? invoices = await _apiEndPointService.getInvoices(
        page: currentPage, searchQuery: searchQuery);
    if (invoices == null) {
      return [];
    }
    return invoices;
  }
}
