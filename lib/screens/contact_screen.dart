import 'package:flutter/material.dart';
import 'package:vf_prototype_flutter/controllers/contact_controller.dart';
import 'package:vf_prototype_flutter/enums/page_url_enum.dart';
import 'package:vf_prototype_flutter/models/contact/contact_model.dart';
import 'package:vf_prototype_flutter/services/route_service.dart';
import 'package:vf_prototype_flutter/utils/country_utils.dart';
import 'package:vf_prototype_flutter/widgets/input_widget.dart';
import 'package:vf_prototype_flutter/widgets/select_widget.dart';

import '../constants/color_constants.dart';
import '../constants/padding_constants.dart';
import '../constants/size_constants.dart';
import '../services/messages_service.dart';

class ContactScreen extends StatefulWidget {
  final ContactModel contactModel;
  final ContactController _contactController = ContactController();
  final MessagesService _messagesService = MessagesService();
  ContactScreen({Key? key, required this.contactModel}) : super(key: key);

  @override
  _ContactScreenState createState() => _ContactScreenState();
}

class _ContactScreenState extends State<ContactScreen> {
  @override
  initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Kontakt'),
        actions: <Widget>[
          Padding(
            padding: kIconPadding,
            child: Visibility(
              visible: widget.contactModel.idContact > 0,
              child: IconButton(
                icon: const Icon(
                  Icons.restore_from_trash,
                  color: kColorIcon,
                ),
                onPressed: () => {_deleteContact()},
              ),
            ),
          ),
          Padding(
            padding: kIconPadding,
            child: IconButton(
              icon: const Icon(
                Icons.save,
                color: kColorIcon,
              ),
              onPressed: getSaveHandler(),
            ),
          ),
        ],
      ),
      body: Builder(
        builder: (context) {
          return Center(
            child: Container(
              constraints: kSizeContainer,
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Card(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          const ListTile(
                            title: Text('Fakturační údaje'),
                          ),
                          InputWidget(
                              labelText: "Váš název",
                              initialValue: widget.contactModel.name,
                              onChange: (String v) => {
                                    widget.contactModel.name = v,
                                    setState(() => {})
                                  }),
                          InputWidget(
                              labelText: "Jméno",
                              initialValue: widget.contactModel.firstname,
                              onChange: (String v) =>
                                  {widget.contactModel.firstname = v}),
                          InputWidget(
                              labelText: "Příjmení",
                              initialValue: widget.contactModel.lastname,
                              onChange: (String v) =>
                                  {widget.contactModel.lastname = v}),
                          InputWidget(
                              labelText: "Ulice",
                              initialValue: widget.contactModel.street,
                              onChange: (String v) =>
                                  {widget.contactModel.street = v}),
                          InputWidget(
                              labelText: "Město",
                              initialValue: widget.contactModel.city,
                              onChange: (String v) =>
                                  {widget.contactModel.city = v}),
                          InputWidget(
                              labelText: "PSČ",
                              initialValue: widget.contactModel.zip,
                              onChange: (String v) =>
                                  {widget.contactModel.zip = v}),
                          InputWidget(
                              labelText: "IČO",
                              initialValue: widget.contactModel.ic,
                              onChange: (String v) =>
                                  {widget.contactModel.ic = v}),
                          InputWidget(
                              labelText: "DIČ",
                              initialValue: widget.contactModel.dic,
                              onChange: (String v) =>
                                  {widget.contactModel.dic = v}),
                          SelectWidget(
                              labelText: "Země",
                              onChange: (dynamic v) => {
                                    widget.contactModel.countryCode =
                                        CountryUtils.getCountryCodeByName(v),
                                  },
                              values: CountryUtils.getCountryNamesForSelect(),
                              value: CountryUtils.getCountryNameByCode(
                                  widget.contactModel.countryCode)),
                        ],
                      ),
                    ),
                    Card(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          const ListTile(
                            title: Text('Dodací údaje'),
                          ),
                          InputWidget(
                              labelText: "Název firmy",
                              initialValue: widget.contactModel.deliveryCompany,
                              onChange: (String v) =>
                                  {widget.contactModel.deliveryCompany = v}),
                          InputWidget(
                              labelText: "Jméno",
                              initialValue:
                                  widget.contactModel.deliveryFirstname,
                              onChange: (String v) =>
                                  {widget.contactModel.deliveryFirstname = v}),
                          InputWidget(
                              labelText: "Příjmení",
                              initialValue:
                                  widget.contactModel.deliveryLastname,
                              onChange: (String v) =>
                                  {widget.contactModel.deliveryLastname = v}),
                          InputWidget(
                              labelText: "Ulice",
                              initialValue: widget.contactModel.deliveryStreet,
                              onChange: (String v) =>
                                  {widget.contactModel.deliveryStreet = v}),
                          InputWidget(
                              labelText: "Město",
                              initialValue: widget.contactModel.deliveryCity,
                              onChange: (String v) =>
                                  {widget.contactModel.deliveryCity = v}),
                          InputWidget(
                              labelText: "PSČ",
                              initialValue: widget.contactModel.deliveryZip,
                              onChange: (String v) =>
                                  {widget.contactModel.deliveryZip = v}),
                          InputWidget(
                              labelText: "Telefon",
                              initialValue: widget.contactModel.deliveryTel,
                              onChange: (String v) =>
                                  {widget.contactModel.deliveryTel = v}),
                          SelectWidget(
                            labelText: "Země",
                            onChange: (dynamic v) => {
                              widget.contactModel.deliveryCountryCode =
                                  CountryUtils.getCountryCodeByName(v)
                            },
                            values: CountryUtils.getCountryNamesForSelect(),
                            value: CountryUtils.getCountryNameByCode(
                                widget.contactModel.deliveryCountryCode),
                          ),
                          InputWidget(
                              labelText: "E-mail",
                              textInputType: TextInputType.emailAddress,
                              initialValue: widget.contactModel.email,
                              onChange: (String v) =>
                                  {widget.contactModel.email = v}),
                        ],
                      ),
                    ),
                    Card(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          const ListTile(
                            title: Text('Texty'),
                          ),
                          InputWidget(
                              labelText: "Netisknutelná poznámka",
                              initialValue: widget.contactModel.note,
                              onChange: (String v) =>
                                  {widget.contactModel.note = v}),
                          InputWidget(
                              labelText: "Doplňkový text",
                              initialValue:
                                  widget.contactModel.textInvoiceFooter,
                              maxLines: 2,
                              onChange: (String v) =>
                                  {widget.contactModel.textInvoiceFooter = v}),
                          InputWidget(
                              labelText: "Text před položkami faktury",
                              initialValue: widget.contactModel.textBeforeItems,
                              maxLines: 2,
                              onChange: (String v) =>
                                  {widget.contactModel.textBeforeItems = v}),
                          InputWidget(
                              labelText: "Text v patičce faktury",
                              initialValue:
                                  widget.contactModel.textInvoiceFooter,
                              maxLines: 2,
                              onChange: (String v) =>
                                  {widget.contactModel.textInvoiceFooter = v}),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  void _deleteContact() {
    widget._contactController.deleteContact(widget.contactModel);
    widget._messagesService
        .showToast(context: context, message: "Kontakt smazán.");
    RouteService.pushRoute(PageUrl.contacts, context);
  }

  void _saveContact() async {
    bool success = false;
    if (widget.contactModel.idContact > 0) {
      ContactModel? newContactModel =
          await widget._contactController.updateContact(widget.contactModel);
      success = newContactModel != null;
    } else {
      success =
          await widget._contactController.createContact(widget.contactModel);
    }
    if (success) {
      widget._messagesService
          .showToast(context: context, message: "Kontakt uložen.");
      RouteService.pushRoute(PageUrl.contacts, context);
    }
  }

  VoidCallback? getSaveHandler() {
    if (widget.contactModel.name.isEmpty) {
      return null;
    }
    return () => _saveContact();
  }
}
