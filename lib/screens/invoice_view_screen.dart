import 'package:flutter/material.dart';
import 'package:vf_prototype_flutter/controllers/invoice_controller.dart';
import 'package:vf_prototype_flutter/controllers/payment_method_controller.dart';
import 'package:vf_prototype_flutter/models/invoice/invoice_item_model.dart';
import 'package:vf_prototype_flutter/models/invoice/invoice_vat_model.dart';
import 'package:vf_prototype_flutter/services/messages_service.dart';
import 'package:vf_prototype_flutter/services/route_service.dart';
import 'package:vf_prototype_flutter/utils/currency_utils.dart';
import 'package:vf_prototype_flutter/utils/invoice_utils.dart';
import 'package:vf_prototype_flutter/utils/reabable_date_utils.dart';
import 'package:vf_prototype_flutter/widgets/badge_list_widget.dart';
import 'package:vf_prototype_flutter/widgets/invoice_item_line_widget.dart';
import 'package:vf_prototype_flutter/widgets/invoice_vat_line_widget.dart';

import '../constants/color_constants.dart';
import '../constants/padding_constants.dart';
import '../constants/size_constants.dart';
import '../enums/page_url_enum.dart';
import '../models/invoice/invoice_model.dart';
import '../widgets/invoice_contact_widget.dart';

class InvoiceViewScreen extends StatefulWidget {
  late InvoiceModel invoiceModel;
  final InvoiceController _invoiceController = InvoiceController();
  final MessagesService _messagesService = MessagesService();
  final CurrencyUtils _currencyUtils = CurrencyUtils();
  final PaymentMethodController _paymentMethodController =
      PaymentMethodController();
  InvoiceViewScreen({Key? key, required this.invoiceModel}) : super(key: key);

  @override
  _InvoiceViewScreenState createState() => _InvoiceViewScreenState();
}

class _InvoiceViewScreenState extends State<InvoiceViewScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.invoiceModel.number),
        actions: <Widget>[
          Padding(
            padding: kIconPadding,
            child: IconButton(
              icon: const Icon(
                Icons.more_vert,
                color: kColorIcon,
              ),
              onPressed: () => {_showMoreActions()},
            ),
          ),
        ],
      ),
      body: Builder(
        builder: (context) => Center(
          child: Container(
            constraints: kSizeContainer,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                      padding: kInvoiceItemBadgesPadding,
                      child: BadgeListWidget(
                        badges: InvoiceUtils.getInvoiceBadges(
                            widget.invoiceModel.flags),
                      )),
                  Card(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Row(
                          children: [
                            InvoiceContactWidget(
                                label: "Dodavatel",
                                name: widget.invoiceModel.supplierName,
                                companyId: widget.invoiceModel.supplierIc,
                                vatId: widget.invoiceModel.supplierDic,
                                street: widget.invoiceModel.supplierStreet,
                                city: widget.invoiceModel.supplierCity,
                                countryCode:
                                    widget.invoiceModel.supplierCountryCode),
                            InvoiceContactWidget(
                                label: "Odběratel",
                                name: widget.invoiceModel.customerName,
                                companyId: widget.invoiceModel.customerIc,
                                vatId: widget.invoiceModel.customerDic,
                                street: widget.invoiceModel.customerStreet,
                                city: widget.invoiceModel.customerCity,
                                countryCode:
                                    widget.invoiceModel.customerCountryCode),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Card(
                    child: Padding(
                      padding: const EdgeInsets.all(20),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Row(
                            children: [
                              Expanded(
                                child: Column(
                                  children: [
                                    Text("Datum vystavení: " +
                                        ReadableDateUtils
                                            .convertApiDateToReadable(widget
                                                .invoiceModel.dateCreated)),
                                    Text("Datum splatnosti: " +
                                        ReadableDateUtils
                                            .convertApiDateToReadable(
                                                widget.invoiceModel.dateDue)),
                                    Text("Platební metoda: " +
                                        widget._paymentMethodController
                                            .getPaymentMethodById(widget
                                                .invoiceModel.idPaymentMethod)!
                                            .name),
                                  ],
                                ),
                              ),
                              Expanded(
                                child: Column(
                                  children: [
                                    Visibility(
                                        child: Text(
                                            "VS: " + widget.invoiceModel.vs),
                                        visible: widget.invoiceModel.vs != ""),
                                    Visibility(
                                        child: Text(
                                            "SS: " + widget.invoiceModel.ss),
                                        visible: widget.invoiceModel.ss != ""),
                                    Visibility(
                                        child: Text(
                                            "KS: " + widget.invoiceModel.ks),
                                        visible: widget.invoiceModel.ks != ""),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  Card(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        const ListTile(
                          title: Text('Položky'),
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: Column(
                                children: const [
                                  Text("Počet kusů"),
                                ],
                              ),
                            ),
                            Expanded(
                              child: Column(
                                children: const [
                                  Text("MJ"),
                                ],
                              ),
                            ),
                            Expanded(
                              child: Column(
                                children: const [
                                  Text("Text"),
                                ],
                              ),
                            ),
                            Expanded(
                              child: Column(
                                children: const [
                                  Text("% DPH"),
                                ],
                              ),
                            ),
                            Expanded(
                              child: Column(
                                children: const [
                                  Text("Cena"),
                                ],
                              ),
                            ),
                          ],
                        ),
                        ListView.builder(
                          shrinkWrap: true,
                          itemCount: widget.invoiceModel.items.length,
                          itemBuilder: (context, index) {
                            return InvoiceItemLineWidget(
                              currencyKey: widget.invoiceModel.currency,
                              invoiceItemModel: InvoiceItemModel(
                                  quantity:
                                      widget.invoiceModel.items[index].quantity,
                                  unit: widget.invoiceModel.items[index].unit,
                                  text: widget.invoiceModel.items[index].text,
                                  vatRate:
                                      widget.invoiceModel.items[index].vatRate,
                                  unitPrice: widget
                                      .invoiceModel.items[index].unitPrice),
                            );
                          },
                        ),
                        const Padding(padding: EdgeInsets.all(16))
                      ],
                    ),
                  ),
                  Card(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        const ListTile(
                          title: Text('Celkem'),
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: Column(
                                children: const [
                                  Text("Sazba"),
                                ],
                              ),
                            ),
                            Expanded(
                              child: Column(
                                children: const [
                                  Text("Základ"),
                                ],
                              ),
                            ),
                            Expanded(
                              child: Column(
                                children: const [
                                  Text("DPH"),
                                ],
                              ),
                            ),
                            Expanded(
                              child: Column(
                                children: const [
                                  Text("Včetně DPH"),
                                ],
                              ),
                            ),
                          ],
                        ),
                        ListView.builder(
                          shrinkWrap: true,
                          itemCount: widget.invoiceModel.vats.length,
                          itemBuilder: (context, index) {
                            return InvoiceVatLineWidget(
                              currencyKey: widget.invoiceModel.currency,
                              invoiceVatModel: InvoiceVatModel(
                                vatRate:
                                    widget.invoiceModel.vats[index].vatRate,
                                base: widget.invoiceModel.vats[index].base,
                                vat: widget.invoiceModel.vats[index].vat,
                                total: widget.invoiceModel.vats[index].total,
                              ),
                            );
                          },
                        ),
                        Container(
                          alignment: Alignment.bottomRight,
                          child: Padding(
                            padding: const EdgeInsets.all(24),
                            child: Text(
                              "Celkem k úhradě: ${widget.invoiceModel.total} ${widget._currencyUtils.getCurrencySymbol(widget.invoiceModel.currency)}",
                              style: const TextStyle(
                                  fontSize: 24, fontWeight: FontWeight.bold),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _deleteInvoice() {
    widget._invoiceController.deleteInvoice(widget.invoiceModel);
    widget._messagesService
        .showToast(context: context, message: "Faktura smazána.");
    RouteService.pushRoute(PageUrl.invoices, context);
  }

  void _stornoInvoice() async {
    await widget._invoiceController
        .stornoInvoice(widget.invoiceModel, !widget.invoiceModel.isStorno());
    String message = "Faktura stornována.";
    if (!widget.invoiceModel.isStorno()) {
      message = "Faktura odstornována.";
    }
    widget._messagesService.showToast(context: context, message: message);
    _updateInvoice();
  }

  void _archiveInvoice() async {
    await widget._invoiceController
        .archiveInvoice(widget.invoiceModel, !widget.invoiceModel.isArchived());
    String message = "Faktura archivována.";
    if (!widget.invoiceModel.isArchived()) {
      message = "Faktura odarchivována.";
    }
    widget._messagesService.showToast(context: context, message: message);
    _updateInvoice();
  }

  void _updateInvoice() async {
    InvoiceModel? newInvoiceModel = await widget._invoiceController
        .getInvoice(widget.invoiceModel.idInvoice);
    if (newInvoiceModel != null) {
      setState(() {
        widget.invoiceModel = newInvoiceModel;
      });
    }
  }

  void _editInvoice() {
    RouteService.pushRouteWithArgs(
        PageUrl.invoiceCreate, widget.invoiceModel, context);
  }

  void _showMoreActions() async {
    await showDialog<bool>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Možnosti dokladu " + widget.invoiceModel.number),
          actions: <Widget>[
            TextButton(
              child: const Text("Upravit"),
              onPressed: () {
                Navigator.of(context).pop(true);
                _editInvoice();
              },
            ),
            TextButton(
              child: const Text("Smazat"),
              onPressed: () {
                Navigator.of(context).pop(true);
                _deleteInvoice();
              },
            ),
            TextButton(
              child: Text((widget.invoiceModel.isArchived())
                  ? "Odarchivovat"
                  : "Archivovat"),
              onPressed: () {
                Navigator.of(context).pop(true);
                _archiveInvoice();
              },
            ),
            TextButton(
              child: Text((widget.invoiceModel.isStorno())
                  ? "Odstornovat"
                  : "Stornovat"),
              onPressed: () {
                Navigator.of(context).pop(true);
                _stornoInvoice();
              },
            ),
            TextButton(
              child: const Text("Otevřít veřejnou stránku dokladu"),
              onPressed: () {
                Navigator.of(context).pop(true);
                widget._invoiceController.openWebPage(widget.invoiceModel);
              },
            ),
          ],
        );
      },
    );
  }
}
