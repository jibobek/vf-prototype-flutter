import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:vf_prototype_flutter/controllers/contact_controller.dart';

import 'package:vf_prototype_flutter/controllers/contact_list_controller.dart';
import 'package:vf_prototype_flutter/controllers/invoice_controller.dart';
import 'package:vf_prototype_flutter/enums/page_url_enum.dart';
import 'package:vf_prototype_flutter/models/invoice/invoice_model.dart';
import 'package:vf_prototype_flutter/models/number_serie/number_serie_model.dart';
import 'package:vf_prototype_flutter/models/payment_method/payment_method_model.dart';
import 'package:vf_prototype_flutter/services/messages_service.dart';
import 'package:vf_prototype_flutter/services/route_service.dart';
import 'package:vf_prototype_flutter/services/storage_service.dart';
import 'package:vf_prototype_flutter/utils/currency_utils.dart';
import 'package:vf_prototype_flutter/widgets/button_widget.dart';
import 'package:vf_prototype_flutter/widgets/input_widget.dart';
import 'package:vf_prototype_flutter/widgets/invoice_item_input_widget.dart';
import 'package:vf_prototype_flutter/widgets/select_widget.dart';

import '../constants/color_constants.dart';
import '../constants/padding_constants.dart';
import '../constants/size_constants.dart';
import '../models/contact/contact_model.dart';
import '../models/invoice/invoice_item_model.dart';

class InvoiceCreateScreen extends StatefulWidget {
  late InvoiceModel invoiceModel;
  final InvoiceController _invoiceController = InvoiceController();
  final ContactListController _contactListController = ContactListController();
  final ContactController _contactController = ContactController();
  final CurrencyUtils _currencyUtils = CurrencyUtils();
  final MessagesService _messagesService = MessagesService();
  ContactModel editCustomer = ContactModel.empty();
  InvoiceCreateScreen({
    Key? key,
    required this.invoiceModel,
  }) : super(key: key);

  @override
  _InvoiceCreateScreenState createState() => _InvoiceCreateScreenState();
}

class _InvoiceCreateScreenState extends State<InvoiceCreateScreen> {
  @override
  initState() {
    super.initState();
    if (widget.invoiceModel.idInvoice <= 0) {
      _prepareEmptyInvoice();
    } else {
      _loadCustomer();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text((widget.invoiceModel.idInvoice > 0)
            ? "Úprava dokladu"
            : "Vystavení dokladu"),
        actions: <Widget>[
          Padding(
            padding: kIconPadding,
            child: IconButton(
              icon: const Icon(
                Icons.save,
                color: kColorIcon,
              ),
              onPressed: () => {_saveInvoice()},
            ),
          ),
        ],
      ),
      body: Builder(
        builder: (context) => Center(
          child: Container(
            constraints: kSizeContainer,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Card(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 16, vertical: 6),
                          child: Row(
                            children: <Widget>[
                              const Expanded(
                                child: Text("Odběratel"),
                              ),
                              Expanded(
                                child: DropdownSearch<ContactModel>(
                                    items: (widget.invoiceModel.idInvoice > 0)
                                        ? [widget.editCustomer]
                                        : [],
                                    selectedItem: widget.editCustomer,
                                    showSearchBox: true,
                                    dropdownSearchDecoration:
                                        const InputDecoration(
                                      border: UnderlineInputBorder(),
                                    ),
                                    searchDelay:
                                        const Duration(milliseconds: 500),
                                    mode: Mode.MENU,
                                    onFind: (String? filter) => widget
                                        ._contactListController
                                        .getContactsByQuery(filter!),
                                    itemAsString:
                                        (ContactModel? contactModel) =>
                                            contactModel!.toSelectString(),
                                    onChanged: (ContactModel? contactModel) => {
                                          widget.invoiceModel.idCustomer =
                                              contactModel!.idContact,
                                          updateCustomer(contactModel)
                                        }),
                              ),
                            ],
                          ),
                        ),
                        InputWidget(
                            labelText: "Číslo dokumentu",
                            initialValue: widget.invoiceModel.number,
                            onChange: (String v) =>
                                {widget.invoiceModel.number = v}),
                        SelectWidget(
                            labelText: "Platební metoda",
                            onChange: (dynamic v) => widget.invoiceModel
                                .idPaymentMethod = v.idPaymentMethod,
                            values: getPaymentMethodsForSelect(),
                            value: getSelectedPaymentMethod()),
                        SelectWidget(
                            labelText: "Číselná řada",
                            onChange: (dynamic v) => widget
                                .invoiceModel.idNumberSeries = v.idNumberSerie,
                            values: getNumberSeriesForSelect(),
                            value: getSelectedNumberSerie()),
                        InputWidget(
                            labelText: "Splatnost (dny)",
                            initialValue:
                                widget.invoiceModel.daysDue.toString(),
                            textInputType: TextInputType.number,
                            onChange: (String v) =>
                                {widget.invoiceModel.daysDue = int.parse(v)}),
                        InputWidget(
                            labelText: "Datum vystavení",
                            textInputType: TextInputType.datetime,
                            onChange: (String v) => {}),
                        SelectWidget(
                            labelText: "Měna",
                            onChange: (dynamic v) =>
                                widget.invoiceModel.currency = v.name,
                            values: widget._currencyUtils.currencies,
                            value: (widget.invoiceModel.currency == "")
                                ? widget._currencyUtils.defaultCurrency
                                : widget._currencyUtils.getCurrencyFromString(
                                    widget.invoiceModel.currency)),
                        InputWidget(
                            labelText: "VS",
                            initialValue: widget.invoiceModel.vs,
                            onChange: (String v) =>
                                {widget.invoiceModel.vs = v}),
                        InputWidget(
                            labelText: "KS",
                            initialValue: widget.invoiceModel.ks,
                            onChange: (String v) =>
                                {widget.invoiceModel.ks = v}),
                        InputWidget(
                            labelText: "SS",
                            initialValue: widget.invoiceModel.ss,
                            onChange: (String v) =>
                                {widget.invoiceModel.ss = v}),
                      ],
                    ),
                  ),
                  Card(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        const ListTile(
                          title: Text('Položky'),
                        ),
                        ListView.builder(
                          shrinkWrap: true,
                          itemCount: widget.invoiceModel.items.length,
                          itemBuilder: ((context, index) {
                            return InvoiceItemInputWidget(
                              onChange: (InvoiceItemModel invoiceItemModel) => {
                                itemsChanged(),
                                widget.invoiceModel.items[index] =
                                    invoiceItemModel
                              },
                              invoiceItemModel: InvoiceItemModel(
                                quantity:
                                    widget.invoiceModel.items[index].quantity,
                                unitPrice:
                                    widget.invoiceModel.items[index].unitPrice,
                                text: widget.invoiceModel.items[index].text,
                                unit: widget.invoiceModel.items[index].unit,
                                vatRate:
                                    widget.invoiceModel.items[index].vatRate,
                              ),
                              onRemove: () {
                                if (widget.invoiceModel.items.length > 1) {
                                  _removeItem(index);
                                } else {
                                  widget._messagesService.showToast(
                                      context: context,
                                      message: "Nelze smazat jedinou položku.");
                                }
                              },
                            );
                          }),
                        ),
                        Center(
                          child: Padding(
                            padding: const EdgeInsets.all(10),
                            child: ButtonWidget(
                              icon: Icons.add,
                              text: "Přidat další",
                              onClicked: () => {_addItem()},
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<void> _loadCustomer() async {
    ContactModel? customer = await widget._contactController
        .getContact(widget.invoiceModel.idCustomer);
    if (customer == null) {
      return;
    }
    setState(() => {widget.editCustomer = customer});
    return;
  }

  List<PaymentMethodModel> getPaymentMethodsForSelect() {
    return StorageService.settingsModel!.paymentMethods;
  }

  PaymentMethodModel getSelectedPaymentMethod() {
    int paymentMethodId = widget.invoiceModel.idPaymentMethod;
    List<PaymentMethodModel> availablePaymentMethods =
        StorageService.settingsModel!.paymentMethods;
    PaymentMethodModel paymentMethodModel = availablePaymentMethods.firstWhere(
        (e) => e.idPaymentMethod == paymentMethodId,
        orElse: () => availablePaymentMethods.first);
    return paymentMethodModel;
  }

  List<NumberSerieModel> getNumberSeriesForSelect() {
    if (StorageService.settingsModel == null) {
      return [];
    }
    return StorageService.settingsModel!.getNumberSeriesByType(1);
  }

  NumberSerieModel getSelectedNumberSerie() {
    int numberSerieId = widget.invoiceModel.idNumberSeries;
    List<NumberSerieModel> availableNumberSeries =
        StorageService.settingsModel!.getNumberSeriesByType(1);
    NumberSerieModel numberSerieModel = availableNumberSeries.firstWhere(
        (e) => e.idNumberSerie == numberSerieId,
        orElse: () => availableNumberSeries.first);
    return numberSerieModel;
  }

  void _addItem() {
    setState(() {
      widget._invoiceController.addEmptyItem(widget.invoiceModel);
    });
  }

  void _removeItem(int index) {
    setState(() {
      widget.invoiceModel.items.removeAt(index);
    });
  }

  void _prepareEmptyInvoice() {
    _addItem();
  }

  void showFailedValidationToast() {
    widget._messagesService.showToast(
        context: context,
        message: "Fakturu nelze uložit. Položka musí obsahovat text a cenu.");
  }

  void _saveInvoice() async {
    if (widget.invoiceModel.idInvoice == 0) {
      int? idInvoice =
          await widget._invoiceController.createInvoice(widget.invoiceModel);
      if (idInvoice == null) {
        showFailedValidationToast();
      } else {
        InvoiceModel? invoiceModel =
            await widget._invoiceController.getInvoice(idInvoice);
        RouteService.pushRouteWithArgs(PageUrl.invoice, invoiceModel!, context);
      }
      return;
    }

    InvoiceModel? updatedInvoice =
        await widget._invoiceController.updateInvoice(widget.invoiceModel);
    if (updatedInvoice == null) {
      showFailedValidationToast();
      return;
    }
    InvoiceModel? invoiceModel =
        await widget._invoiceController.getInvoice(updatedInvoice.idInvoice);
    if (invoiceModel == null) {
      return;
    }
    RouteService.pushRouteWithArgs(PageUrl.invoice, invoiceModel, context);
  }

  void updateCustomer(ContactModel contactModel) {
    widget.invoiceModel = widget._invoiceController
        .setCustomerToInvoice(widget.invoiceModel, contactModel);
  }

  void itemsChanged() {
    if (StorageService.isCompanyVatPayer == false) {
      for (InvoiceItemModel item in widget.invoiceModel.items) {
        item.vatRate = 0;
      }
    }
  }
}
