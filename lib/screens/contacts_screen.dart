import 'package:flutter/material.dart';
import 'package:vf_prototype_flutter/controllers/contact_list_controller.dart';
import 'package:vf_prototype_flutter/enums/page_url_enum.dart';
import 'package:vf_prototype_flutter/models/contact/contact_model.dart';
import 'package:vf_prototype_flutter/services/route_service.dart';
import 'package:vf_prototype_flutter/widgets/contact_list_section_widget.dart';

import '../constants/color_constants.dart';
import '../constants/padding_constants.dart';
import '../constants/size_constants.dart';

class ContactsScreen extends StatefulWidget {
  final ContactListController _contactListController = ContactListController();
  Map<String, List<ContactModel>> displayedContacts = {};
  late ScrollController _scrollController;
  bool isLoading = true;
  ContactsScreen({Key? key}) : super(key: key);

  @override
  _ContactsScreenState createState() => _ContactsScreenState();
}

class _ContactsScreenState extends State<ContactsScreen> {
  @override
  initState() {
    super.initState();
    widget._scrollController = ScrollController();
    widget._scrollController.addListener(_scrollListener);
    _loadContacts();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Adresář'),
        actions: <Widget>[
          Padding(
            padding: kIconPadding,
            child: IconButton(
              icon: const Icon(
                Icons.add,
                color: kColorIcon,
              ),
              onPressed: () => {_newContact()},
            ),
          ),
        ],
      ),
      body: Builder(
        builder: (context) => Center(
          child: Container(
            constraints: kSizeContainer,
            child: SingleChildScrollView(
              controller: widget._scrollController,
              child: Column(
                children: [
                  ListView.builder(
                      shrinkWrap: true,
                      itemCount: widget.displayedContacts.values.length,
                      itemBuilder: (context, index) {
                        String key =
                            widget.displayedContacts.keys.elementAt(index);
                        return ContactListSectionWidget(
                            contacts: widget.displayedContacts[key] ?? [],
                            letter: key,
                            onTap: (ContactModel contactModel) => {
                                  RouteService.pushRouteWithArgs(
                                      PageUrl.contact, contactModel, context)
                                });
                      }),
                  Visibility(
                    visible: widget.isLoading,
                    child: const Center(
                      child: Padding(
                        padding: EdgeInsets.all(16),
                        child: CircularProgressIndicator(),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _loadContacts() async {
    List<ContactModel> contacts =
        await widget._contactListController.getContacts();
    widget.displayedContacts =
        widget._contactListController.groupContactsByLetter(contacts);
    widget.isLoading = false;
    setState(() => {});
  }

  void _loadNext() async {
    widget.isLoading = true;
    setState(() => {});
    List<ContactModel> newContacts =
        await widget._contactListController.getNextContacts();
    Map<String, List<ContactModel>> newContactsGrouped =
        widget._contactListController.groupContactsByLetter(newContacts);
    widget.displayedContacts.addAll(newContactsGrouped);
    widget.isLoading = false;
    setState(() => {});
  }

  _scrollListener() {
    if (widget._scrollController.offset >=
            widget._scrollController.position.maxScrollExtent &&
        !widget._scrollController.position.outOfRange) {
      _loadNext();
    }
  }

  _newContact() {
    RouteService.pushRouteWithArgs(
        PageUrl.contact, ContactModel.empty(), context);
  }
}
