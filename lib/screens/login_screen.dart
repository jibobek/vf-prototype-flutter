import 'package:flutter/material.dart';
import 'package:vf_prototype_flutter/controllers/login_controller.dart';
import 'package:vf_prototype_flutter/enums/page_url_enum.dart';
import 'package:vf_prototype_flutter/services/messages_service.dart';
import 'package:vf_prototype_flutter/services/route_service.dart';
import 'package:vf_prototype_flutter/widgets/button_widget.dart';

import '../constants/size_constants.dart';

class LoginScreen extends StatefulWidget {
  String email = "";
  String password = "";
  final LoginController _loginController = LoginController();
  final MessagesService _messagesService = MessagesService();
  LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  initState() {
    super.initState();
    _trySavedLogin();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          title: const Text('Přihlášení'),
        ),
        body: Center(
          child: Container(
            height: 350,
            constraints: kSizeContainer,
            child: Padding(
              padding: const EdgeInsets.all(30),
              child: Card(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const Padding(
                        padding: EdgeInsets.all(24),
                        child: Align(
                          alignment: Alignment.bottomLeft,
                          child: Text(
                            "Přihlášení do aplikace",
                            style: TextStyle(fontSize: 18),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 24, vertical: 0),
                        child: TextField(
                          onChanged: (String? value) {
                            widget.email = value!;
                          },
                          keyboardType: TextInputType.emailAddress,
                          decoration: const InputDecoration(
                            label: Text("E-mail"),
                            prefixIcon: Icon(Icons.email),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 24, vertical: 0),
                        child: TextField(
                          obscureText: true,
                          onChanged: (String? value) {
                            widget.password = value!;
                          },
                          keyboardType: TextInputType.text,
                          decoration: const InputDecoration(
                            label: Text("Heslo"),
                            prefixIcon: Icon(Icons.lock),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 24, vertical: 8),
                        child: ButtonWidget(
                          icon: Icons.login,
                          text: "Přihlásit",
                          onClicked: () => _login(),
                        ),
                      ),
                    ]),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<void> _login() async {
    bool reqState = await widget._loginController
        .loginWithPassword(widget.email, widget.password);
    setState(() {
      widget.password = "";
    });
    if (reqState) {
      RouteService.pushRoute(PageUrl.home, context);
    } else {
      widget._messagesService.showToast(
          context: context,
          message: "Přihlášení se nezdařilo. Zkontrolujte přihlašovací údaje.");
    }
  }

  void _trySavedLogin() async {
    bool autologinState = await widget._loginController.trySavedLogin();
    if (autologinState) {
      RouteService.pushRoute(PageUrl.home, context);
    }
  }
}
