import 'package:flutter/material.dart';

import '../constants/size_constants.dart';
import '../widgets/navigation_drawer_widget.dart';

class HomeScreen extends StatefulWidget {
  static const double _padding = 20;
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        drawer: NavigationDrawerWidget(),
        appBar: AppBar(
          title: const Text("Domovská stránka"),
          // automaticallyImplyLeading: false,
        ),
        body: Builder(
          builder: (context) {
            return Center(
              child: Container(
                constraints: kSizeContainer,
                child: Card(
                  clipBehavior: Clip.antiAlias,
                  child: Column(
                    children: const [
                      ListTile(
                        title: Text('Úvod'),
                      ),
                      Align(
                        child: Padding(
                            padding: EdgeInsets.all(HomeScreen._padding),
                            child: Text(
                                'Vítejte v aplikaci. Pro provedení akcí využijte tlačítka v menu.')),
                        alignment: Alignment.bottomLeft,
                      ),
                      Align(
                        child: Padding(
                            padding: EdgeInsets.all(HomeScreen._padding),
                            child:
                                Text('Využité technologie:\n• Flutter 2.10.4')),
                        alignment: Alignment.bottomLeft,
                      )
                    ],
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
