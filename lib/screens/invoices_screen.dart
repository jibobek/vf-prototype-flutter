import 'package:flutter/material.dart';
import 'package:vf_prototype_flutter/controllers/invoice_list_controller.dart';
import 'package:vf_prototype_flutter/enums/page_url_enum.dart';
import 'package:vf_prototype_flutter/models/invoice/invoice_model.dart';
import 'package:vf_prototype_flutter/services/route_service.dart';
import 'package:vf_prototype_flutter/utils/currency_utils.dart';
import 'package:vf_prototype_flutter/utils/invoice_utils.dart';
import 'package:vf_prototype_flutter/widgets/invoice_item_widget.dart';
import 'package:vf_prototype_flutter/widgets/invoices_list_header_widget.dart';
import 'package:vf_prototype_flutter/widgets/searchbar_widget.dart';

import '../constants/color_constants.dart';
import '../constants/padding_constants.dart';
import '../constants/size_constants.dart';
import '../utils/reabable_date_utils.dart';

class InvoicesScreen extends StatefulWidget {
  final InvoiceListController _invoiceListController = InvoiceListController();
  List<InvoiceModel> displayedInvoices = [];
  late ScrollController _scrollController;
  final CurrencyUtils _currencyUtils = CurrencyUtils();
  late TextEditingController searchController;
  String _searchQuery = "";
  bool isLoading = true;
  InvoicesScreen({Key? key}) : super(key: key);

  @override
  _InvoicesScreenState createState() => _InvoicesScreenState();
}

class _InvoicesScreenState extends State<InvoicesScreen> {
  @override
  initState() {
    super.initState();
    widget._scrollController = ScrollController();
    widget._scrollController.addListener(_scrollListener);
    widget.searchController = TextEditingController(text: widget._searchQuery);
    _loadInvoices();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Doklady'),
        actions: <Widget>[
          Padding(
            padding: kIconPadding,
            child: IconButton(
              icon: const Icon(
                Icons.add,
                color: kColorIcon,
              ),
              onPressed: () =>
                  {RouteService.pushRoute(PageUrl.invoiceCreate, context)},
            ),
          ),
        ],
      ),
      body: Builder(
        builder: (context) => Center(
          child: Container(
            constraints: kSizeContainer,
            child: SingleChildScrollView(
              controller: widget._scrollController,
              child: Column(
                children: [
                  SearchbarWidget(
                      textEditingController: widget.searchController,
                      onChange: (String? value) =>
                          {widget._searchQuery = value!, _loadInvoices()}),
                  const InvoiceListHeaderWidget(),
                  ListView.builder(
                    shrinkWrap: true,
                    itemCount: widget.displayedInvoices.length,
                    itemBuilder: (context, index) {
                      return InvoiceItemWidget(
                        invoiceNumber: widget.displayedInvoices[index].number,
                        customerName:
                            widget.displayedInvoices[index].customerName,
                        dateCreated: ReadableDateUtils.convertApiDateToReadable(
                            widget.displayedInvoices[index].dateCreated),
                        dateDue: ReadableDateUtils.convertApiDateToReadable(
                            widget.displayedInvoices[index].dateDue),
                        price: widget.displayedInvoices[index].total +
                            " " +
                            widget._currencyUtils.getCurrencySymbol(
                                widget.displayedInvoices[index].currency),
                        onTap: () =>
                            {_openInvoice(widget.displayedInvoices[index])},
                        badges: InvoiceUtils.getInvoiceBadges(
                            widget.displayedInvoices[index].flags),
                      );
                    },
                  ),
                  Visibility(
                    visible: widget.isLoading,
                    child: const Center(
                      child: Padding(
                        padding: EdgeInsets.all(16),
                        child: CircularProgressIndicator(),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _loadInvoices() async {
    widget.isLoading = true;
    setState(() => {});
    widget.displayedInvoices =
        await widget._invoiceListController.getInvoices(widget._searchQuery);
    widget.isLoading = false;
    setState(() => {});

    widget.searchController.selection = TextSelection.fromPosition(
        TextPosition(offset: widget.searchController.text.length));
  }

  void _loadNext() async {
    widget.isLoading = true;
    setState(() => {});
    List<InvoiceModel> newInvoices = await widget._invoiceListController
        .getNextInvoices(widget._searchQuery);
    for (InvoiceModel c in newInvoices) {
      widget.displayedInvoices.add(c);
    }
    widget.isLoading = false;
    setState(() => {});
  }

  _scrollListener() {
    if (widget._scrollController.offset >=
            widget._scrollController.position.maxScrollExtent &&
        !widget._scrollController.position.outOfRange) {
      _loadNext();
    }
  }

  _openInvoice(InvoiceModel invoiceModel) {
    RouteService.pushRouteWithArgs(PageUrl.invoice, invoiceModel, context);
  }
}
