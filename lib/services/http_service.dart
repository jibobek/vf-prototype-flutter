import 'package:dio/dio.dart';

import 'package:vf_prototype_flutter/constants/url_constants.dart';
import 'package:vf_prototype_flutter/services/storage_service.dart';

class HttpService {
  late Dio _dio;

  HttpService() {
    _dio = Dio(
      BaseOptions(
        baseUrl: kApiUrl,
      ),
    );
    initializeInterceptors();
  }

  Future<Response> getRequest(
      {required String endPoint,
      Map<String, dynamic>? queryParameters,
      bool authentication = true}) async {
    Response response;

    Options options = Options();
    if (authentication) {
      options = Options(headers: {
        "Authorization": "Basic " + StorageService.getCurrentApiKey()
      });
    }

    try {
      response = await _dio.get(kApiUrl + endPoint,
          queryParameters: queryParameters, options: options);
    } on DioError catch (e) {
      throw Exception(e.message);
    }

    return response;
  }

  Future<Response> postRequest(
      {required String endPoint,
      required data,
      Map<String, dynamic>? queryParameters,
      bool authentication = true}) async {
    Response response;
    Options options = Options();
    if (authentication) {
      options = Options(headers: {
        "Authorization": "Basic " + StorageService.getCurrentApiKey()
      });
    }

    try {
      response = await _dio.post(kApiUrl + endPoint,
          data: data, queryParameters: queryParameters, options: options);
    } on DioError catch (e) {
      throw Exception(e.message);
    }

    return response;
  }

  Future<Response> putRequest(
      {required String endPoint,
      required data,
      Map<String, dynamic>? queryParameters,
      bool authentication = true}) async {
    Response response;

    Options options = Options();
    if (authentication) {
      options = Options(headers: {
        "Authorization": "Basic " + StorageService.getCurrentApiKey()
      });
    }
    try {
      response = await _dio.put(kApiUrl + endPoint,
          data: data, queryParameters: queryParameters, options: options);
    } on DioError catch (e) {
      throw Exception(e.message);
    }
    return response;
  }

  Future<Response> deleteRequest(
      {required String endPoint,
      Map<String, dynamic>? queryParameters,
      bool authentication = true}) async {
    Response response;

    Options options = Options();
    if (authentication) {
      options = Options(headers: {
        "Authorization": "Basic " + StorageService.getCurrentApiKey()
      });
    }
    try {
      response = await _dio.delete(kApiUrl + endPoint,
          queryParameters: queryParameters, options: options);
    } on DioError catch (e) {
      throw Exception(e.message);
    }
    return response;
  }

  initializeInterceptors() {
    _dio.interceptors.add(InterceptorsWrapper(
      onRequest: (request, handler) {
        return handler.next(request);
      },
      onResponse: (response, handler) {
        return handler.next(response);
      },
      onError: (error, handler) {
        return handler.next(error);
      },
    ));
  }
}
