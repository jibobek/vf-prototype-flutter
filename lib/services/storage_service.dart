import 'dart:convert';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:vf_prototype_flutter/enums/storage_key_enum.dart';
import 'package:vf_prototype_flutter/models/login/company_model.dart';

import '../models/settings/settings_model.dart';

class StorageService {
  static final FlutterSecureStorage _permanentStorage = FlutterSecureStorage();
  static String _currentApiKey = "";
  static String currentUserEmail = "";
  static int _currentCompany = 0;
  static bool isCompanyVatPayer = true;
  static SettingsModel? settingsModel;

  static String getCurrentApiKey() {
    return _currentApiKey;
  }

  static void setCurrentApiKey(String apiKey) {
    _currentApiKey = apiKey;
    _savePermanentStorageData();
  }

  static Future<void> loadPernamentStorage() async {
    String? jsonData =
        await _permanentStorage.read(key: StorageKey.login.asString);
    if (jsonData == null) {
      return;
    }
    Map<String, dynamic> data = jsonDecode(jsonData);
    currentUserEmail = data["email"];
    _currentApiKey = data["api_key"];
    changeCurrentCompany(data["current_company"]);
  }

  static Future<void> _savePermanentStorageData() async {
    String jsonData = jsonEncode({
      "email": currentUserEmail,
      "api_key": _currentApiKey,
      "current_company": _currentCompany
    });
    await _permanentStorage.write(
        key: StorageKey.login.asString, value: jsonData);
  }

  static Future<void> clearStorage() async {
    await _permanentStorage.deleteAll();
    _currentApiKey = "";
    currentUserEmail = "";
    changeCurrentCompany(0);
    settingsModel = null;
  }

  static changeCurrentCompany(int companyId) {
    CompanyModel? companyModel = settingsModel?.companies
        .firstWhere((element) => element.idCompany == companyId);
    if (companyModel != null) {
      isCompanyVatPayer = companyModel.flags == 1;
    }
    _currentCompany = companyId;
  }

  static int getCurrentCompanyId() {
    return _currentCompany;
  }
}
