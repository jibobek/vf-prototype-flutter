import 'package:flutter/material.dart';
import 'package:vf_prototype_flutter/enums/page_url_enum.dart';
import 'package:vf_prototype_flutter/models/contact/contact_model.dart';
import 'package:vf_prototype_flutter/models/invoice/invoice_model.dart';
import 'package:vf_prototype_flutter/screens/home_screen.dart';
import 'package:vf_prototype_flutter/screens/invoice_create_screen.dart';
import 'package:vf_prototype_flutter/screens/invoice_view_screen.dart';
import 'package:vf_prototype_flutter/screens/login_screen.dart';

import '../screens/contact_screen.dart';
import '../screens/contacts_screen.dart';
import '../screens/invoices_screen.dart';

class RouteService {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;
    String? url = settings.name;

    if (url == PageUrl.home.asString) {
      return MaterialPageRoute(builder: (_) => const HomeScreen());
    }

    if (url == PageUrl.contact.asString && args is ContactModel) {
      return MaterialPageRoute(
          builder: (_) => ContactScreen(contactModel: args));
    }

    if (url == PageUrl.contacts.asString) {
      return MaterialPageRoute(builder: (_) => ContactsScreen());
    }

    if (url == PageUrl.invoiceCreate.asString && args is InvoiceModel) {
      return MaterialPageRoute(
          builder: (_) => InvoiceCreateScreen(invoiceModel: args));
    }

    if (url == PageUrl.invoiceCreate.asString) {
      return MaterialPageRoute(
          builder: (_) =>
              InvoiceCreateScreen(invoiceModel: InvoiceModel.empty()));
    }

    if (url == PageUrl.invoice.asString && args is InvoiceModel) {
      return MaterialPageRoute(
          builder: (_) => InvoiceViewScreen(invoiceModel: args));
    }

    if (url == PageUrl.invoices.asString) {
      return MaterialPageRoute(builder: (_) => InvoicesScreen());
    }

    if (url == PageUrl.login.asString) {
      return MaterialPageRoute(builder: (_) => LoginScreen());
    }

    return _errorRoute();
  }

  static void pushRoute(PageUrl pageUrl, BuildContext context) {
    Navigator.pushNamed(context, pageUrl.asString);
  }

  static void pushRouteWithArgs(
      PageUrl pageUrl, Object args, BuildContext context) {
    Navigator.pushNamed(context, pageUrl.asString, arguments: args);
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          title: const Text('404'),
        ),
      );
    });
  }
}
