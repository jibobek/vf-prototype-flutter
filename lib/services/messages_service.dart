import 'package:flutter/material.dart';

class MessagesService {
  void showToast(
      {required BuildContext context,
      required String message,
      SnackBarAction? action}) {
    final scaffold = ScaffoldMessenger.of(context);
    scaffold.showSnackBar(
      SnackBar(
        content: Text(message),
        action: action,
      ),
    );
  }
}
