import 'package:dio/dio.dart';
import 'package:vf_prototype_flutter/models/contact/contact_model.dart';
import 'package:vf_prototype_flutter/models/invoice/invoice_model.dart';
import 'package:vf_prototype_flutter/models/login/autologin_model.dart';
import 'package:vf_prototype_flutter/models/login/login_model.dart';
import 'package:vf_prototype_flutter/services/http_service.dart';

import 'http_service.dart';

class ApiEndPointService {
  final HttpService _httpService = HttpService();

  ApiEndPointService();

  Future<LoginModel?> loginWithPassword(String email, String password) async {
    try {
      Response response = await _httpService.postRequest(
          endPoint: "/login", data: {"email": email, "password": password});
      LoginModel loginModel = LoginModel.fromJson(response.data);
      return loginModel;
    } on Exception {
      return null;
    }
  }

  Future<AutoLoginModel?> testApiHash() async {
    try {
      Response response = await _httpService.postRequest(
          endPoint: "/mobile-autologin",
          data: {"version": "1.1.0"},
          authentication: true);
      AutoLoginModel autoLoginModel = AutoLoginModel.fromJson(response.data);
      return autoLoginModel;
    } on Exception {
      return null;
    }
  }

  Future<List<ContactModel>?> getContacts(
      {int page = 1, String searchQuery = ""}) async {
    const int itemsPerRequest = 25;
    int offset = (page - 1) * itemsPerRequest;
    try {
      Response response =
          await _httpService.getRequest(endPoint: "/contact", queryParameters: {
        "rows_limit": itemsPerRequest,
        "rows_offset": offset,
        "sort": "name~asc",
        "q": searchQuery
      });

      List<ContactModel> contacts = [];
      for (var item in response.data) {
        contacts.add(ContactModel.fromJson(item));
      }
      return contacts;
    } on Exception {
      return null;
    }
  }

  Future<List<InvoiceModel>?> getInvoices(
      {int page = 1, String searchQuery = ""}) async {
    const int itemsPerRequest = 10;
    int offset = (page - 1) * itemsPerRequest;
    try {
      Response response = await _httpService.getRequest(
          endPoint: "/invoice",
          queryParameters: {
            "rows_limit": itemsPerRequest,
            "rows_offset": offset,
            "q": searchQuery
          });

      List<InvoiceModel> invoices = [];
      for (var item in response.data) {
        invoices.add(InvoiceModel.fromJson(item));
      }
      return invoices;
    } on Exception {
      return null;
    }
  }

  Future<InvoiceModel?> getInvoice(int invoiceId) async {
    try {
      Response response = await _httpService.getRequest(
          endPoint: "/invoice/" + invoiceId.toString());
      return InvoiceModel.fromJson(response.data);
    } on Exception {
      return null;
    }
  }

  Future<ContactModel?> getContact(int contactId) async {
    try {
      Response response = await _httpService.getRequest(
          endPoint: "/contact/" + contactId.toString());
      return ContactModel.fromJson(response.data);
    } on Exception {
      return null;
    }
  }

  Future<bool> deleteContact(ContactModel contactModel) async {
    try {
      await _httpService.deleteRequest(
          endPoint: "/contact/" + contactModel.idContact.toString());
      return true;
    } on Exception {
      return false;
    }
  }

  Future<ContactModel?> updateContact(ContactModel contactModel) async {
    try {
      Response response = await _httpService.putRequest(
          data: contactModel.toJson(),
          endPoint: "/contact/" + contactModel.idContact.toString());
      return ContactModel.fromJson(response.data);
    } on Exception {
      return null;
    }
  }

  Future<bool> createContact(ContactModel contactModel) async {
    try {
      Response response = await _httpService.postRequest(
          data: contactModel.toJson(), endPoint: "/contact");
      return true;
    } on Exception {
      return false;
    }
  }

  Future<bool> deleteInvoice(InvoiceModel invoiceModel) async {
    try {
      await _httpService.deleteRequest(
          endPoint: "/invoice/" + invoiceModel.idInvoice.toString());
      return true;
    } on Exception {
      return false;
    }
  }

  Future<InvoiceModel?> updateInvoice(InvoiceModel invoiceModel) async {
    try {
      Response response = await _httpService.putRequest(
          data: invoiceModel.toJson(),
          endPoint: "/invoice/" + invoiceModel.idInvoice.toString());
      return InvoiceModel.fromJson(response.data);
    } on Exception {
      return null;
    }
  }

  Future<int?> createInvoice(InvoiceModel invoiceModel) async {
    try {
      Response response = await _httpService.postRequest(
          data: invoiceModel.toJson(), endPoint: "/invoice");
      return response.data["id"];
    } on Exception {
      return null;
    }
  }

  Future<bool> stornoInvoice(InvoiceModel invoiceModel, bool storno) async {
    try {
      String urlParam = storno ? "storno" : "undo-storno";
      await _httpService.postRequest(
          data: {},
          endPoint: "/invoice/" +
              invoiceModel.idInvoice.toString() +
              "/do/" +
              urlParam +
              "/");
      return true;
    } on Exception {
      return false;
    }
  }

  Future<bool> archiveInvoice(InvoiceModel invoiceModel, bool archive) async {
    try {
      String urlParam = archive ? "archive" : "undo-archive";
      await _httpService.postRequest(
          data: {},
          endPoint: "/invoice/" +
              invoiceModel.idInvoice.toString() +
              "/do/" +
              urlParam +
              "/");
      return true;
    } on Exception {
      return false;
    }
  }
}
