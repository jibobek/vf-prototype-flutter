enum StorageKey { login }

extension StorageKeyKey on StorageKey {
  String get asString {
    switch (this) {
      case StorageKey.login:
        return "login-key";
      default:
        return "";
    }
  }
}
