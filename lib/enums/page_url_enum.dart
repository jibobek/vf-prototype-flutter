enum PageUrl { invoiceCreate, invoices, contacts, contact, invoice, home, login }

extension PageUrlKey on PageUrl {
  String get asString {
    switch (this) {
      case PageUrl.invoiceCreate:
        return "/invoice-create";
      case PageUrl.invoices:
        return "/invoices";
      case PageUrl.contacts:
        return "/contacts";
      case PageUrl.contact:
        return "/contact";
      case PageUrl.invoice:
        return "/invoice";
      case PageUrl.login:
        return "/login";
      case PageUrl.home:
        return "/home";

      default:
        return "/";
    }
  }
}
