import 'package:flutter/material.dart';

import '../models/invoice/invoice_item_model.dart';
import '../utils/currency_utils.dart';

class InvoiceItemLineWidget extends StatelessWidget {
  final InvoiceItemModel invoiceItemModel;
  final CurrencyUtils _currencyUtils = CurrencyUtils();
  late String currencyKey;
  late String _currencySymbol;
  InvoiceItemLineWidget(
      {Key? key, required this.invoiceItemModel, required this.currencyKey}) : super(key: key) {
    _currencySymbol = _currencyUtils.getCurrencySymbol(currencyKey);
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Column(
            children: [
              Text(invoiceItemModel.quantity.toString()),
            ],
          ),
        ),
        Expanded(
          child: Column(
            children: [
              Text(invoiceItemModel.unit),
            ],
          ),
        ),
        Expanded(
          child: Column(
            children: [
              Text(invoiceItemModel.text),
            ],
          ),
        ),
        Expanded(
          child: Column(
            children: [
              Text(invoiceItemModel.vatRate.toString()),
            ],
          ),
        ),
        Expanded(
          child: Column(
            children: [
              Text(invoiceItemModel.unitPrice.toString() +
                  " " +
                  _currencySymbol),
            ],
          ),
        ),
      ],
    );
  }
}
