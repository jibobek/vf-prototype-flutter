import 'package:flutter/material.dart';
import 'package:vf_prototype_flutter/models/contact/contact_model.dart';

import 'contact_item_widget.dart';

class ContactListSectionWidget extends StatelessWidget {
  final String letter;
  final List<ContactModel> contacts;
  final ValueSetter<ContactModel> onTap;

  const ContactListSectionWidget(
      {Key? key,
      required this.letter,
      required this.contacts,
      required this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ListTile(
            title: Text(letter,
                style: const TextStyle(fontSize: 18, color: Colors.white))),
        ListView.builder(
          shrinkWrap: true,
          itemCount: contacts.length,
          itemBuilder: (context, index) {
            return ContactItemWidget(
                name: contacts[index].name,
                address: _getContactAddress(contacts[index]),
                email: contacts[index].email,
                note: contacts[index].note,
                onTap: () => {onTap(contacts[index])});
          },
        ),
      ],
    );
  }

  String _getContactAddress(ContactModel contactModel) {
    return contactModel.street +
        ((contactModel.street != "" && contactModel.city != "") ? ", " : "") +
        contactModel.city;
  }
}
