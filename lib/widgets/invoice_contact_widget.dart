import 'package:flutter/material.dart';

class InvoiceContactWidget extends StatelessWidget {
  final String label;
  final String name;
  final String companyId;
  final String vatId;
  final String street;
  final String city;
  final String countryCode;

  const InvoiceContactWidget(
      {Key? key,
      required this.label,
      required this.name,
      required this.companyId,
      required this.vatId,
      required this.street,
      required this.city,
      required this.countryCode})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Column(
        children: [
          ListTile(
            title: Text(label),
            subtitle: Text("$name\n$companyId\n$vatId\n$street\n$city\n$countryCode"),
          ),
        ],
      ),
    );
  }
}
