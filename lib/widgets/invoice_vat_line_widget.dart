import 'package:flutter/material.dart';
import 'package:vf_prototype_flutter/utils/currency_utils.dart';

import '../models/invoice/invoice_vat_model.dart';

class InvoiceVatLineWidget extends StatelessWidget {
  final InvoiceVatModel invoiceVatModel;
  final String currencyKey;
  final CurrencyUtils _currencyUtils = CurrencyUtils();
  late String _currencySymbol;
  InvoiceVatLineWidget(
      {Key? key, required this.invoiceVatModel, required this.currencyKey}) : super(key: key) {
    _currencySymbol = _currencyUtils.getCurrencySymbol(currencyKey);
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Column(
            children: [
              Text(invoiceVatModel.vatRate.toString() + " %"),
            ],
          ),
        ),
        Expanded(
          child: Column(
            children: [
              Text(invoiceVatModel.base.toString() + " " + _currencySymbol),
            ],
          ),
        ),
        Expanded(
          child: Column(
            children: [
              Text(invoiceVatModel.vat.toString() + " " + _currencySymbol),
            ],
          ),
        ),
        Expanded(
          child: Column(
            children: [
              Text(invoiceVatModel.total.toString() + " " + _currencySymbol),
            ],
          ),
        ),
      ],
    );
  }
}
