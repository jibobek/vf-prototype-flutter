import 'package:flutter/material.dart';
import 'package:vf_prototype_flutter/constants/size_constants.dart';

import '../constants/padding_constants.dart';
import 'badge_widget.dart';

class BadgeListWidget extends StatelessWidget {
  final Map<String, Color> badges;

  const BadgeListWidget({Key? key, required this.badges}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool screenWidthBig =
        MediaQuery.of(context).size.width > kSizeMobileBreakpoint;
    bool smallVariant = _getBadges().length < 4 || screenWidthBig;
    return Column(
      children: [
        Visibility(
          child: Padding(
            padding: kInvoiceItemBadgesPadding,
            child: Row(
              children: _getBadges(),
            ),
          ),
          visible: smallVariant,
        ),
        Visibility(
          visible: smallVariant == false,
          child: Padding(
            padding: kInvoiceItemBadgesPadding,
            child: GridView.count(
                childAspectRatio: 3,
                shrinkWrap: true,
                crossAxisCount: 4,
                crossAxisSpacing: 4.0,
                mainAxisSpacing: 8.0,
                children: List.generate(
                    _getBadges().length, (index) => _getBadges()[index])),
          ),
        ),
      ],
    );
  }

  List<Widget> _getBadges() {
    List<Widget> widgets = badges.entries
        .map((map) => BadgeWidget(label: map.key, colorBackground: map.value))
        .toList();
    return widgets;
  }
}
