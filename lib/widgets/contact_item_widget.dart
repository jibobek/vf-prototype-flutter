import 'package:flutter/material.dart';

import '../constants/padding_constants.dart';

class ContactItemWidget extends StatefulWidget {
  final String name;
  final String address;
  final String email;
  final String note;
  final VoidCallback onTap;
  const ContactItemWidget(
      {Key? key,
      required this.name,
      required this.address,
      required this.email,
      required this.note,
      required this.onTap})
      : super(key: key);

  @override
  _ContactItemWidgetState createState() => _ContactItemWidgetState();
}

class _ContactItemWidgetState extends State<ContactItemWidget> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: kContactItemPadding,
      child: Card(
        child: ListTile(
          leading: const Icon(Icons.person, size: 56),
          title: Text(widget.name),
          subtitle: Text("${widget.address}\n${widget.email}\n${widget.note}"),
          isThreeLine: true,
          onTap: () => widget.onTap(),
        ),
      ),
    );
  }
}
