import 'package:flutter/material.dart';
import 'package:vf_prototype_flutter/services/storage_service.dart';

import '../constants/size_constants.dart';
import '../models/invoice/invoice_item_model.dart';

class InvoiceItemInputWidget extends StatefulWidget {
  final VoidCallback onRemove;
  final Function(InvoiceItemModel) onChange;
  final InvoiceItemModel invoiceItemModel;
  const InvoiceItemInputWidget(
      {Key? key,
      required this.onRemove,
      required this.invoiceItemModel,
      required this.onChange})
      : super(key: key);

  @override
  _InvoiceItemInputWidgetState createState() => _InvoiceItemInputWidgetState();
}

class _InvoiceItemInputWidgetState extends State<InvoiceItemInputWidget> {
  @override
  Widget build(BuildContext context) {
    bool screenWidthBig =
        MediaQuery.of(context).size.width > kSizeMobileBreakpoint;
    double padding = (screenWidthBig) ? 16 : 4;
    return Row(children: [
      Expanded(
        child: Padding(
          padding: EdgeInsets.all(padding),
          child: TextFormField(
            initialValue: widget.invoiceItemModel.quantity.toString(),
            keyboardType: TextInputType.number,
            onChanged: (String? value) => {
              if (value != null && value != "")
                {
                  widget.invoiceItemModel.quantity = double.parse(value),
                  widget.onChange(widget.invoiceItemModel)
                }
            },
            decoration: const InputDecoration(
              border: UnderlineInputBorder(),
              labelText: 'Počet kusů',
            ),
          ),
        ),
      ),
      Expanded(
        child: Padding(
          padding: EdgeInsets.all(padding),
          child: TextFormField(
            initialValue: widget.invoiceItemModel.unit,
            keyboardType: TextInputType.number,
            onChanged: (String? value) => {
              widget.invoiceItemModel.unit = value!,
              widget.onChange(widget.invoiceItemModel)
            },
            decoration: const InputDecoration(
              border: UnderlineInputBorder(),
              labelText: 'MJ',
            ),
          ),
        ),
      ),
      Expanded(
        child: Padding(
          padding: EdgeInsets.all(padding),
          child: TextFormField(
            initialValue: widget.invoiceItemModel.text,
            keyboardType: TextInputType.number,
            onChanged: (String? value) => {
              widget.invoiceItemModel.text = value!,
              widget.onChange(widget.invoiceItemModel)
            },
            decoration: const InputDecoration(
              border: UnderlineInputBorder(),
              labelText: 'Popis',
            ),
          ),
        ),
      ),
      Visibility(
        visible: StorageService.isCompanyVatPayer,
        child: Expanded(
          child: Padding(
            padding: const EdgeInsets.only(top: 4),
            child: DropdownButtonFormField<String>(
              decoration: const InputDecoration(
                  isDense: true,
                  border: UnderlineInputBorder(),
                  labelText: 'DPH'),
              value: widget.invoiceItemModel.vatRate.toString(),
              onChanged: (String? newValue) {
                widget.invoiceItemModel.vatRate = int.parse(newValue!);
                widget.onChange(widget.invoiceItemModel);
              },
              items: ["21", "15", "10", "0"]
                  .map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(value + " %"),
                );
              }).toList(),
            ),
          ),
        ),
      ),
      Expanded(
        child: Padding(
          padding: EdgeInsets.all(padding),
          child: TextFormField(
            initialValue: widget.invoiceItemModel.unitPrice.toString(),
            keyboardType: TextInputType.number,
            onChanged: (String? value) => {
              if (value != null && value != "")
                {
                  widget.invoiceItemModel.unitPrice = double.parse(value),
                  widget.onChange(widget.invoiceItemModel),
                }
            },
            decoration: const InputDecoration(
              border: UnderlineInputBorder(),
              labelText: 'Cena',
            ),
          ),
        ),
      ),
      Padding(
        padding: EdgeInsets.all(padding),
        child: TextButton.icon(
          onPressed: widget.onRemove,
          icon: const Icon(Icons.remove_circle),
          label: const Text(""),
        ),
      ),
    ]);
  }
}
