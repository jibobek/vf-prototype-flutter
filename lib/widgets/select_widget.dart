import 'package:flutter/material.dart';

import '../constants/color_constants.dart';

class SelectWidget<Selectable> extends StatefulWidget {
  Selectable value;
  final List<Selectable> values;
  final String labelText;
  final ValueSetter<dynamic> onChange;

  SelectWidget(
      {Key? key,
      required this.labelText,
      required this.onChange,
      required this.values,
      required this.value})
      : super(key: key);

  @override
  _SelectWidgetState createState() => _SelectWidgetState<Selectable>();
}

class _SelectWidgetState<Selectable> extends State<SelectWidget> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 4),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Text(widget.labelText),
          ),
          Expanded(
            child: DropdownButton<Selectable>(
              isExpanded: true,
              value: widget.value,
              elevation: 24,
              underline: Container(height: 2, color: kColorInputUnderline),
              onChanged: (Selectable? newValue) {
                widget.onChange(newValue);
                setState(() {
                  widget.value = newValue;
                });
              },
              items: widget.values.map<DropdownMenuItem<Selectable>>((value) {
                return DropdownMenuItem<Selectable>(
                  value: value,
                  child: Text(value.toString()),
                );
              }).toList(),
            ),
          ),
        ],
      ),
    );
  }
}
