import 'package:flutter/material.dart';
import 'package:vf_prototype_flutter/constants/color_constants.dart';
import 'package:vf_prototype_flutter/controllers/login_controller.dart';
import 'package:vf_prototype_flutter/models/login/company_model.dart';
import 'package:vf_prototype_flutter/services/route_service.dart';
import 'package:vf_prototype_flutter/services/storage_service.dart';
import '../enums/page_url_enum.dart';

class NavigationDrawerWidget extends StatelessWidget {
  final padding = const EdgeInsets.symmetric(horizontal: 20);
  final LoginController _loginController = new LoginController();

  NavigationDrawerWidget({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Material(
        color: kColorMenuBackground,
        child: ListView(
          children: <Widget>[
            buildHeader(
                companies: StorageService.settingsModel!.companies,
                selectedCompany: StorageService.getCurrentCompanyId(),
                email: StorageService.currentUserEmail,
                context: context),
            Container(
              padding: padding,
              child: Column(
                children: [
                  const SizedBox(height: 24),
                  buildMenuItem(
                    text: 'Domů',
                    icon: Icons.home,
                    onClicked: () =>
                        selectedItem(context, PageUrl.home.asString),
                  ),
                  const SizedBox(height: 16),
                  buildMenuItem(
                    text: 'Vystavit doklad',
                    icon: Icons.inventory_outlined,
                    onClicked: () =>
                        selectedItem(context, PageUrl.invoiceCreate.asString),
                  ),
                  const SizedBox(height: 16),
                  buildMenuItem(
                    text: 'Doklady',
                    icon: Icons.inventory,
                    onClicked: () =>
                        selectedItem(context, PageUrl.invoices.asString),
                  ),
                  const SizedBox(height: 16),
                  buildMenuItem(
                    text: 'Zákazníci',
                    icon: Icons.people,
                    onClicked: () =>
                        selectedItem(context, PageUrl.contacts.asString),
                  ),
                  const SizedBox(height: 24),
                  const Divider(color: kColorMenuDivider),
                  const SizedBox(height: 24),
                  buildMenuItem(
                    text: 'Odhlásit se',
                    icon: Icons.logout,
                    onClicked: () => _logout(context),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildHeader(
          {required List<CompanyModel> companies,
          required int selectedCompany,
          required String email,
          required BuildContext context}) =>
      InkWell(
        child: Container(
          padding: padding.add(const EdgeInsets.symmetric(vertical: 40)),
          child: Row(
            children: [
              const SizedBox(width: 20),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  DropdownButton<CompanyModel>(
                    dropdownColor: kColorMenuBackground,
                    value: companies.firstWhere(
                        (element) => element.idCompany == selectedCompany),
                    elevation: 24,
                    onChanged: (CompanyModel? newCompany) {
                      StorageService.changeCurrentCompany(
                          newCompany!.idCompany);
                      _loginController.loadCompanyData();
                      Navigator.pop(context);
                    },
                    items:
                        companies.map<DropdownMenuItem<CompanyModel>>((value) {
                      return DropdownMenuItem<CompanyModel>(
                        value: value,
                        child: Text(
                          value.name,
                          style: const TextStyle(
                              fontSize: 18, color: Colors.white),
                        ),
                      );
                    }).toList(),
                  ),
                  const SizedBox(height: 4),
                  Text(
                    email,
                    style: const TextStyle(fontSize: 14, color: Colors.white),
                  ),
                ],
              ),
            ],
          ),
        ),
      );

  Widget buildMenuItem({
    required String text,
    required IconData icon,
    VoidCallback? onClicked,
  }) {
    return ListTile(
      leading: Icon(icon, color: kColorMenuText),
      title: Text(text, style: const TextStyle(color: kColorMenuText)),
      hoverColor: kColorMenuHover,
      onTap: onClicked,
    );
  }

  void selectedItem(BuildContext context, String pageUrl) {
    Navigator.of(context).pop();
    Navigator.of(context).pushNamed(pageUrl);
  }

  Future<void> _logout(BuildContext context) async {
    RouteService.pushRoute(PageUrl.login, context);
    await StorageService.clearStorage();
    return;
  }
}
