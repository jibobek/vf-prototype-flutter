import 'package:flutter/material.dart';

import '../constants/color_constants.dart';

class BadgeWidget extends StatelessWidget {
  final String label;
  final Color colorBackground;
  const BadgeWidget(
      {Key? key, required this.label, required this.colorBackground})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 4, vertical: 0),
      child: Container(
        width: 100.0,
        height: 30.0,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(24.0),
          color: colorBackground,
        ),
        child: Center(
          child: Text(
            label,
            style: const TextStyle(
              color: kColorBadgeText,
              fontSize: 12,
              height: 1,
            ),
            textAlign: TextAlign.center,
          ),
        ),
      ),
    );
  }
}
