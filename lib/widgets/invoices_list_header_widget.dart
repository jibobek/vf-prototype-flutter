import 'package:flutter/material.dart';

import '../constants/color_constants.dart';
import '../constants/padding_constants.dart';

class InvoiceListHeaderWidget extends StatelessWidget {
  const InvoiceListHeaderWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Row(
        children: const [
          Expanded(
            child: Padding(
              padding: kInvoiceItemAttributePadding,
              child: Text("Odběratel",
                  style: TextStyle(color: kColorInvoiceItemAtribute)),
            ),
          ),
          Expanded(
            child: Padding(
              padding: kInvoiceItemAttributePadding,
              child: Text("Datum vystavení",
                  style: TextStyle(color: kColorInvoiceItemAtribute)),
            ),
          ),
          Expanded(
            child: Padding(
              padding: kInvoiceItemAttributePadding,
              child: Text("Datum splatnosti",
                  style: TextStyle(color: kColorInvoiceItemAtribute)),
            ),
          ),
          Expanded(
            child: Padding(
              padding: kInvoiceItemAttributePadding,
              child: Text("Cena",
                  style: TextStyle(color: kColorInvoiceItemAtribute)),
            ),
          ),
        ],
      ),
    );
  }
}
