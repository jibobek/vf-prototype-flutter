import 'package:flutter/material.dart';

class InputWidget extends StatefulWidget {
  final String labelText;
  final ValueSetter<String> onChange;
  int? maxLines;
  final TextInputType textInputType;
  final String? initialValue;
  InputWidget(
      {Key? key,
      required this.labelText,
      required this.onChange,
      this.maxLines,
      this.textInputType = TextInputType.text,
      this.initialValue})
      : super(key: key);

  @override
  _InputWidgetState createState() => _InputWidgetState();
}

class _InputWidgetState extends State<InputWidget> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 4),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Text(widget.labelText),
          ),
          Expanded(
            child: TextFormField(
              initialValue: widget.initialValue,
              keyboardType: widget.textInputType,
              onChanged: (String? value) => {widget.onChange(value!)},
              decoration: const InputDecoration(
                border: UnderlineInputBorder(),
              ),
              maxLines: widget.maxLines ?? 1,
            ),
          ),
        ],
      ),
    );
  }
}
