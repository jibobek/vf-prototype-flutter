import 'package:flutter/material.dart';

import '../utils/debouncer.dart';

class SearchbarWidget extends StatefulWidget {
  final ValueSetter<String> onChange;
  final TextEditingController textEditingController;
  final _debouncer = Debouncer(milliseconds: 500);
  SearchbarWidget({
    Key? key,
    required this.onChange,
    required this.textEditingController,
  }) : super(key: key);

  @override
  _SearchbarWidgetState createState() => _SearchbarWidgetState();
}

class _SearchbarWidgetState extends State<SearchbarWidget> {
  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        leading: const Icon(Icons.search),
        trailing: IconButton(
          icon: const Icon(Icons.cancel),
          onPressed: () =>
              {widget.textEditingController.text = "", widget.onChange("")},
        ),
        title: TextFormField(
          controller: widget.textEditingController,
          onChanged: (String? value) => {
            widget._debouncer.run(() => {widget.onChange(value!)}),
          },
          decoration: const InputDecoration(
            border: InputBorder.none,
            labelText: 'Hledat…',
          ),
        ),
      ),
    );
  }
}
