import 'package:flutter/material.dart';

class SplitView extends StatelessWidget {
  Widget menu;
  Widget page;
  SplitView({
    Key? key,
    required this.menu,
    required this.page,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    const breakpoint = 600.0;
    if (screenWidth >= breakpoint) {
      return Row(
        children: [
          SizedBox(
            width: 240,
            child: menu,
          ),
          Container(width: 0.5, color: Colors.black),
          Expanded(
            child: page,
          ),
        ],
      );
    } else {
      return Scaffold(
        body: page,
        drawer: SizedBox(
          width: 240,
          child: Drawer(
            child: menu,
          ),
        ),
      );
    }
  }
}
