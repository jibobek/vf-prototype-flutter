import 'package:flutter/material.dart';
import 'package:vf_prototype_flutter/widgets/badge_list_widget.dart';

import '../constants/color_constants.dart';
import '../constants/padding_constants.dart';
import 'badge_widget.dart';

class InvoiceItemWidget extends StatelessWidget {
  final String invoiceNumber;
  final String customerName;
  final String dateCreated;
  final String dateDue;
  final String price;
  final VoidCallback onTap;
  final Map<String, Color> badges;
  const InvoiceItemWidget(
      {Key? key,
      required this.invoiceNumber,
      required this.customerName,
      required this.dateCreated,
      required this.dateDue,
      required this.price,
      required this.badges,
      required this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => onTap(),
      child: Padding(
        padding: kContactItemPadding,
        child: Card(
            child: Column(
          children: [
            ListTile(
              title: Text(
                invoiceNumber,
                style: const TextStyle(fontSize: 18),
              ),
            ),
            BadgeListWidget(badges: badges),
            Row(
              children: [
                Expanded(
                  child: Padding(
                    padding: kInvoiceItemAttributePadding,
                    child: Text(customerName,
                        style:
                            const TextStyle(color: kColorInvoiceItemAtribute)),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: kInvoiceItemAttributePadding,
                    child: Text(dateCreated,
                        style:
                            const TextStyle(color: kColorInvoiceItemAtribute)),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: kInvoiceItemAttributePadding,
                    child: Text(dateDue,
                        style:
                            const TextStyle(color: kColorInvoiceItemAtribute)),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: kInvoiceItemAttributePadding,
                    child: Text(price,
                        style:
                            const TextStyle(color: kColorInvoiceItemAtribute)),
                  ),
                ),
              ],
            ),
          ],
        )),
      ),
    );
  }
}
